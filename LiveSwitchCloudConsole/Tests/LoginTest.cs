using System;
using System.Reflection;
using Xunit;

namespace LiveSwitchCloudConsole
{
    [Collection("Setup Collection")]
    public class LoginTest : TestBase
    {
        [Fact]
        [BeforeAfterTests]
        public void CanLogin()
        {
            Db().Filter(MethodBase.GetCurrentMethod().Name);
            LauchBrowser();
            GoTo("Login");

            LoginPage login = new LoginPage(driver);
            login.UserCredentials();

            status = login.Success();
            Assert.True(status, "User could not successfully login");
        }

        [Fact]
        [BeforeAfterTests]
        public void CanLogin_FM()
        {
            Db().Filter("FM",MethodBase.GetCurrentMethod().Name);
            LauchBrowser();
            GoTo("Login");

            LoginPage login = new LoginPage(driver);
            login.UserCredentials();
            HomeScreen homeobj = new HomeScreen(driver);
            status = homeobj.GetMyAccounttxt();
            Assert.True(status);
        }
        
    }
}
