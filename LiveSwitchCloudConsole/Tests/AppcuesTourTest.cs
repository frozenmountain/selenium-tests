﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Xunit;

namespace LiveSwitchCloudConsole
{
    [Collection("Setup Collection")]
    public class AppcuesTourTest : TestBase
    {
        [Fact]
        [BeforeAfterTests]
        public void AppcuesIsEnabled()
        {
            Db().Filter();
            LauchBrowser();


            /*GoTo("Signup");

            String Rng = Rand();

            SignUpPage Signup = new SignUpPage(driver);
            Signup.ClickOnCookies();
            Signup.EnterUserName(NewUser(Rng));
            Signup.EnterPassword(Db().GetField("Password"));
            Signup.EnterFirstName(Db().GetField("Firstname"));
            Signup.EnterLastName(Db().GetField("Lastname"));
            Signup.EnterPhoneNumber(Db().GetField("PhoneNumber"));
            Signup.EnterCompany(Db().GetField("Company"));
            Signup.SelectCountry(Db().GetField("Country"));
            Signup.ClickSubmitButton();

            LoginPage Login = Signup.NavigateToLogin();

            Login.NewUserCredentials(NewUser(Rng));*/


            GoTo("Login");

            LoginPage login = new LoginPage(driver);
            login.UserCredentials();


            //new SubscriptionModal(driver).ActivateSubscription();

            AppcuesComponent Appcues = new AppcuesComponent(driver);

            //Appcues.CloseModal();

            status = Appcues.ChecklistIsDisplayed();
            Assert.True(status, "Appcues checklist does not display as a new user");
           
        }
    }
}
