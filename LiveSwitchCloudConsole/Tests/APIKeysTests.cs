﻿using System.Reflection;
using System.Threading;
using Xunit;

namespace LiveSwitchCloudConsole
{
    [Collection("Setup Collection")]

    public class APIKeysTests : TestBase
    {
       
        LoginPage loginObj;
        
        HomeScreen homeObj;
        APIKeys_ApplicationPage apiKeysObj;

        [Fact]
        [BeforeAfterTests]
        public void APIKeys_AddKeys()
        {
            Db().Filter(MethodBase.GetCurrentMethod().Name);
            LauchBrowser();
            GoTo("Login");
   
            loginObj = new LoginPage(driver);
            loginObj.UserCredentials();            

            homeObj = new HomeScreen(driver);
            homeObj.ClickOnAPIKeys();

            apiKeysObj = new APIKeys_ApplicationPage(driver);
            apiKeysObj.ClickOnNew();
            string tagName = Rand();
            apiKeysObj.EnterTagName(tagName);
            apiKeysObj.ClickOnOk();
            
            status = apiKeysObj.ValidateAPIKeyData(tagName);
            
            Assert.True(status);

        }

        

        [Fact]
        [BeforeAfterTests]
        public void APIKeys_UpdateKeys()
        {
            Db().Filter(MethodBase.GetCurrentMethod().Name);
            LauchBrowser();
            GoTo("Login");

            loginObj = new LoginPage(driver);
            loginObj.UserCredentials();
           
            homeObj = new HomeScreen(driver); 
          
            homeObj.ClickOnAPIKeys();

            apiKeysObj = new APIKeys_ApplicationPage(driver);

            apiKeysObj.ClickOnNew();
            string tagName = Rand();
            apiKeysObj.EnterTagName(tagName);
            
            apiKeysObj.ClickOnOk();

            string newTagName = Rand();
            apiKeysObj.ClickOnEditButton(tagName);


            apiKeysObj.EnterTagName(newTagName);
            apiKeysObj.ClickOnOk();
            apiKeysObj.ClickOnSubmit();       
            status = apiKeysObj.ValidateAPIKeyData(newTagName);
            Assert.True(status);

        }

        [Fact]
        [BeforeAfterTests]
        public void APIKeys_DeleteKeys()
        {
            Db().Filter(MethodBase.GetCurrentMethod().Name);
            LauchBrowser();
            GoTo("Login");

            loginObj = new LoginPage(driver);
            loginObj.UserCredentials();

            homeObj = new HomeScreen(driver);
           
            homeObj.ClickOnAPIKeys();

            apiKeysObj = new APIKeys_ApplicationPage(driver);
            apiKeysObj.ClickOnNew();
            string tagName = Rand();
            apiKeysObj.EnterTagName(tagName);

            apiKeysObj.ClickOnOk();

          
            apiKeysObj.ClickOnDeleteButtonAPIKeys(tagName);
            apiKeysObj.ClickOnDeleteConfirm();
        
            status = apiKeysObj.ClickOnDeleteOKButton();

            Assert.True(status);

        }
             
      
    }
    }
