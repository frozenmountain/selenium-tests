﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Xunit;

namespace LiveSwitchCloudConsole
{
    [Collection("Setup Collection")]
    public class SubscriptionTest : TestBase
    {

        [Fact]
        [BeforeAfterTests]
        public void CreateNewSubscription()
        {
            Db().Filter("RC", this.GetType().Name);
            LauchBrowser();
            GoTo("Signup");

            String Rng = Rand();

            SignUpPage Signup = new SignUpPage(driver);
            Signup.ClickOnCookies();
            Signup.EnterUserName(NewUser(Rng));
            Signup.EnterPassword(Db().GetField("Password"));
            Signup.EnterFirstName(Db().GetField("Firstname"));
            Signup.EnterLastName(Db().GetField("Lastname"));
            Signup.EnterPhoneNumber(Db().GetField("PhoneNumber"));
            Signup.EnterCompany(Db().GetField("Company"));
            Signup.SelectCountry(Db().GetField("Country"));
            Signup.ClickSubmitButton();

            LoginPage Login = Signup.NavigateToLogin();

            Login.NewUserCredentials(NewUser(Rng));

            SubscriptionModal SubscriptionModal = new SubscriptionModal(driver);

            SubscriptionModal.ActivateSubscription();

            new AppcuesComponent(driver).CheckAppcuesNps();

            status = SubscriptionModal.ActivationSuccess();
            Assert.True(status, "Unable to create a trial subscription");

        }

        
        [Fact]
        [BeforeAfterTests]
        public void CancelSubscription()
        {
            Db().Filter("RC", this.GetType().Name);
            LauchBrowser();
            GoTo("Signup");

            String Rng = Rand();

            SignUpPage Signup = new SignUpPage(driver);
            Signup.ClickOnCookies();
            Signup.EnterUserName(NewUser(Rng));
            Signup.EnterPassword(Db().GetField("Password"));
            Signup.EnterFirstName(Db().GetField("Firstname"));
            Signup.EnterLastName(Db().GetField("Lastname"));
            Signup.EnterPhoneNumber(Db().GetField("PhoneNumber"));
            Signup.EnterCompany(Db().GetField("Company"));
            Signup.SelectCountry(Db().GetField("Country"));
            Signup.ClickSubmitButton();

            LoginPage Login = Signup.NavigateToLogin();

            Login.NewUserCredentials(NewUser(Rng));

            SubscriptionModal SubscriptionModal = new SubscriptionModal(driver);

            SubscriptionModal.ActivateSubscription();

            new AppcuesComponent(driver).CheckAppcuesNps();

            SubscriptionModal.CloseModal();

            SubscriptionPage SubscriptionPage = new SubscriptionPage(driver);

            SubscriptionPage.Cancel();

            status = SubscriptionPage.CancelledSubscriptionSuccess();
            Assert.True(status, "Unable to read subscription status to be cancelled");
        }

        //DEV ENV ONLY
        [Fact]
        [BeforeAfterTests]
        public void ReactivateSubscription()
        {
            Db().Filter("RC", this.GetType().Name);
            LauchBrowser();
            GoTo("Signup");

            String Rng = Rand();

            SignUpPage Signup = new SignUpPage(driver);
            Signup.ClickOnCookies();
            Signup.EnterUserName(NewUser(Rng));
            Signup.EnterPassword(Db().GetField("Password"));
            Signup.EnterFirstName(Db().GetField("Firstname"));
            Signup.EnterLastName(Db().GetField("Lastname"));
            Signup.EnterPhoneNumber(Db().GetField("PhoneNumber"));
            Signup.EnterCompany(Db().GetField("Company"));
            Signup.SelectCountry(Db().GetField("Country"));
            Signup.ClickSubmitButton();

            LoginPage Login = Signup.NavigateToLogin();

            Login.NewUserCredentials(NewUser(Rng));

            SubscriptionModal SubscriptionModal = new SubscriptionModal(driver);

            SubscriptionModal.ActivateSubscription();

            new AppcuesComponent(driver).CheckAppcuesNps();

            SubscriptionModal.CloseModal();

            SubscriptionPage SubscriptionPage = new SubscriptionPage(driver);

            SubscriptionPage.Cancel();

            new HomeScreen(driver).GoToDashboard();

            SubscriptionModal.ReactivateSubscription();

            status = SubscriptionPage.ActiveStatus();
            Assert.True(status, "Unable to reactivate subscription");
        }

        //DEV ENV ONLY
        [Fact]
        [BeforeAfterTests]
        public void UpdateSubscriptionSupportPlan()
        {
            Db().Filter("RC", this.GetType().Name);
            LauchBrowser();
            GoTo("Signup");

            String Rng = Rand();

            SignUpPage Signup = new SignUpPage(driver);
            Signup.ClickOnCookies();
            Signup.EnterUserName(NewUser(Rng));
            Signup.EnterPassword(Db().GetField("Password"));
            Signup.EnterFirstName(Db().GetField("Firstname"));
            Signup.EnterLastName(Db().GetField("Lastname"));
            Signup.EnterPhoneNumber(Db().GetField("PhoneNumber"));
            Signup.EnterCompany(Db().GetField("Company"));
            Signup.SelectCountry(Db().GetField("Country"));
            Signup.ClickSubmitButton();

            LoginPage Login = Signup.NavigateToLogin();

            Login.NewUserCredentials(NewUser(Rng));

            SubscriptionModal SubscriptionModal = new SubscriptionModal(driver);

            SubscriptionModal.ActivateSubscription();

            new AppcuesComponent(driver).CheckAppcuesNps();

            SubscriptionModal.CloseModal();

            SubscriptionPage SubscriptionPage = new SubscriptionPage(driver);

            SubscriptionPage.Cancel();

            new HomeScreen(driver).GoToDashboard();

            SubscriptionModal.ReactivateSubscription();

            //SubscriptionPage.GoToPage();

            SubscriptionPage.ChangeSupport();

            status = SubscriptionPage.SupportStatus();

            Assert.True(status, "Support status does not have the correct type");

        }
    }
}
