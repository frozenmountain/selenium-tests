﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace LiveSwitchCloudConsole
{
    [Collection("Setup Collection")]
    public class AdminTest :TestBase
    {
        [Fact]
        [BeforeAfterTests]
        public void CanUpdateAdminInformation()
        {
            Db().Filter();
            LauchBrowser();
            GoTo("Login");

            LoginPage Login = new LoginPage(driver);
            Login.UserCredentials();

            ProfilePage Profile = new HomeScreen(driver).GoToProfile();

            Profile.Update();

            status = Profile.UpdateSuccess();
            Assert.True(status, "Profile was not updated successfully");

        }
    }
}
