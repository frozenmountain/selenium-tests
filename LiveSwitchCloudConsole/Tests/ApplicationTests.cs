﻿using System.Reflection;
using Xunit;

namespace LiveSwitchCloudConsole
{
    [Collection("Setup Collection")]

    public class ApplicationTests : TestBase
    { 
        LoginPage loginObj;
        HomeScreen homeObj;
        APIKeys_ApplicationPage apiKeysObj;
       

        [Fact]
        [BeforeAfterTests]
        public void Application_Add()
        {
            Db().Filter(MethodBase.GetCurrentMethod().Name);
            LauchBrowser();
            GoTo("Login");

            loginObj = new LoginPage(driver);
            loginObj.UserCredentials();

            homeObj = new HomeScreen(driver);
            homeObj.ClickOnApplication();


            apiKeysObj = new APIKeys_ApplicationPage(driver);
            apiKeysObj.ClickOnNew();
            string tagName = Rand();
            apiKeysObj.EnterTagName(tagName);
            
            apiKeysObj.ClickOnOk();
            
            status = apiKeysObj.ValidateAPIKeyData(tagName);

            Assert.True(status);

        }


        [Fact]
        [BeforeAfterTests]
        public void Application_Update()
        {
            Db().Filter(MethodBase.GetCurrentMethod().Name);
            LauchBrowser();
            GoTo("Login");

            loginObj = new LoginPage(driver);
            loginObj.UserCredentials();

            homeObj = new HomeScreen(driver);
            homeObj.ClickOnApplication();

            apiKeysObj = new APIKeys_ApplicationPage(driver);
            apiKeysObj.ClickOnNew();
            string tagName = Rand();
            apiKeysObj.EnterTagName(tagName);
            apiKeysObj.ClickOnOk();
            string newTagName = Rand();
            apiKeysObj.ClickOnApplicationEdit(tagName);
                        
            apiKeysObj.EnterNewTagName(newTagName);
            apiKeysObj.ClickOnSubmit();
            homeObj.ClickOnApplication();
            apiKeysObj = new APIKeys_ApplicationPage(driver);
            status = apiKeysObj.ValidateAPIKeyData(newTagName);

            Assert.True(status);

        }

        [Fact]
        [BeforeAfterTests]
        public void Application_Delete()
        {
            Db().Filter(MethodBase.GetCurrentMethod().Name);
            LauchBrowser();
            GoTo("Login");

            loginObj = new LoginPage(driver);
            loginObj.UserCredentials();

            homeObj = new HomeScreen(driver);
            homeObj.ClickOnApplication();

            apiKeysObj = new APIKeys_ApplicationPage(driver);
            apiKeysObj.ClickOnNew();
            string tagName = Rand();
            apiKeysObj.EnterTagName(tagName);
            apiKeysObj.ClickOnOk();

            apiKeysObj.ClickOnDeleteButtonApplication(tagName);
            apiKeysObj.ClickOnDeleteConfirm();
            status = apiKeysObj.ClickOnDeleteOKButton();
            
            Assert.True(status);

        }
    


    }
}
