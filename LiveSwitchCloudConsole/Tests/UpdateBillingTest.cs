﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Xunit;

namespace LiveSwitchCloudConsole

{
    [Collection("Setup Collection")]
    public class UpdateBillingTest : TestBase
    {

        LoginPage loginObj;
        HomeScreen homeObj;      
        BillingPage billingObj;

        [Fact]
        [BeforeAfterTests]
        public void UpdateInvoiceDetails()
        {
            Db().Filter(MethodBase.GetCurrentMethod().Name);
            LauchBrowser();
            GoTo("Login");

            loginObj = new LoginPage(driver);
            loginObj.UserCredentials();

            homeObj = new HomeScreen(driver);
            homeObj.FrozenMountainDropDown();
            homeObj.ClickOnBilling();

            billingObj = new BillingPage(driver);
            billingObj.UpdateOrganization(testDataService["Organization"].ToString());
            billingObj.UpdateInvoiceAddress1(testDataService["Address1"].ToString());
            billingObj.UpdateInvoiceAddress2(testDataService["Address2"].ToString());
            billingObj.UpdateInvoiceCountry(testDataService["Country"].ToString());
            billingObj.UpdateInvoiceState(testDataService["State"].ToString());
            billingObj.UpdateInvoiceCity(testDataService["City"].ToString());
            billingObj.UpdateInvoicePostalCode(Rng(100000, 999999));
            billingObj.ClickOnSubmit();

            status = billingObj.UpdateMessageDisplay();
            Assert.True(status);

        }


        [Fact]
        [BeforeAfterTests]
        public void UpdatePaymentDetails()
        {
            Db().Filter(MethodBase.GetCurrentMethod().Name);
            LauchBrowser();
            GoTo("Login");

            loginObj = new LoginPage(driver);
            loginObj.UserCredentials();

            homeObj = new HomeScreen(driver);
            homeObj.FrozenMountainDropDown();
            homeObj.ClickOnBilling();

            billingObj = new BillingPage(driver);
            billingObj.UpdatePaymentDetailsFirstName(testDataService["FirstName"].ToString());
            billingObj.UpdatePaymentDetailLastName(testDataService["LastName"].ToString());
            billingObj.UpdatePaymentDetailsCardNumber(testDataService["CardNumber"].ToString());
            billingObj.UpdatePaymentDetailsExpiryMonth(testDataService["Month"].ToString());
            billingObj.UpdatePaymentDetailsExpiryYear(testDataService["Year"].ToString());
            billingObj.UpdatePaymentDetailsExpiryCvv(Rng(000, 999));
            
            billingObj.ClickOnSubmit();

            status = billingObj.UpdateMessageDisplay();
            Assert.True(status);

        }


        [Fact]
        [BeforeAfterTests]
        public void UpdatePaymentAddressDetails()
        {
            Db().Filter(MethodBase.GetCurrentMethod().Name);
            LauchBrowser();
            GoTo("Login");


            loginObj = new LoginPage(driver);
            loginObj.UserCredentials();

            homeObj = new HomeScreen(driver);
            homeObj.FrozenMountainDropDown();
            homeObj.ClickOnBilling();

            billingObj = new BillingPage(driver);
            billingObj.scrollDown();
            billingObj.UpdatePaymentAddress1(testDataService["Address1"].ToString());
            billingObj.UpdatePaymentAddress2(testDataService["Address2"].ToString());
            billingObj.UpdatePaymentCountry(testDataService["Country"].ToString());
            billingObj.UpdatePaymentState(testDataService["State"].ToString());
            billingObj.UpdatePaymentCity(testDataService["City"].ToString());
            billingObj.UpdatePaymentPostalCode(Rng(100000, 999999));
            billingObj.ClickOnSubmit();

            status = billingObj.UpdateMessageDisplay();
            Assert.True(status);

        }
    }
}
