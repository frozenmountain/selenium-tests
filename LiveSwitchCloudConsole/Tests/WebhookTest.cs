﻿using System;
using System.Reflection;
using Xunit;

namespace LiveSwitchCloudConsole
{
    [Collection("Setup Collection")]
    public class WebhookTest : TestBase
    {
        HomeScreen homeObj;
        APIKeys_ApplicationPage apiKeysObj;
        ChannelPage editAppObj;
        EditChannelPage editChannelObj;

        [Fact]
        [BeforeAfterTests]
        public void CreateApplicationWebhookTest()
        {
            Db().Filter();
            LauchBrowser();
            GoTo("Login");

            LoginPage login = new LoginPage(driver);
            login.UserCredentials();

            
            homeObj = new HomeScreen(driver);
            homeObj.ClickOnApplication();

            //new AppcuesComponent(driver).CheckAppcuesNps();

            apiKeysObj = new APIKeys_ApplicationPage(driver);
            apiKeysObj.ClickOnNew();
            string tagName = Rand();
            apiKeysObj.EnterTagName(tagName);
            apiKeysObj.ClickOnOk();

            apiKeysObj.ClickOnApplicationEdit(tagName);

            ApplicationsWebhookPage applications = new ApplicationsWebhookPage(driver);


            applications.FilterDb("CreateApplicationWebhookTest");
                        
            applications.CreateNewWebhook();

            
        }

        [Fact]
        [BeforeAfterTests]
        public void UpdateApplicationWebhookTest()
        {
            Db().Filter();
            LauchBrowser();
            GoTo("Login");

            LoginPage login = new LoginPage(driver);
            login.UserCredentials();

            
            homeObj = new HomeScreen(driver);
            homeObj.ClickOnApplication();

            //new AppcuesComponent(driver).CheckAppcuesNps();

            apiKeysObj = new APIKeys_ApplicationPage(driver);
            apiKeysObj.ClickOnNew();
            string tagName = Rand();
            apiKeysObj.EnterTagName(tagName);
            apiKeysObj.ClickOnOk();

            apiKeysObj.ClickOnApplicationEdit(tagName);

            ApplicationsWebhookPage applications = new ApplicationsWebhookPage(driver);


            applications.FilterDb("CreateApplicationWebhookTest");
            applications.CreateNewWebhook();

            applications.FilterDb("UpdateApplicationWebhookTest");
            applications.UpdateWebhook();

            //UPDATE ASSERTION
            //status = new WebhookComponent(driver).UpdateSuccess();
            //Assert.True(status, "Applications Webhook was not able to save");
        }

        [Fact]
        [BeforeAfterTests]
        public void DeleteApplicationWebhookTest()
        {
            Db().Filter();
            LauchBrowser();
            GoTo("Login");

            LoginPage login = new LoginPage(driver);
            WebhookComponent webhook = new WebhookComponent(driver);

            login.UserCredentials();

            homeObj = new HomeScreen(driver);
            homeObj.ClickOnApplication();

            //new AppcuesComponent(driver).CheckAppcuesNps();

            apiKeysObj = new APIKeys_ApplicationPage(driver);
            apiKeysObj.ClickOnNew();
            string tagName = Rand();
            apiKeysObj.EnterTagName(tagName);
            apiKeysObj.ClickOnOk();

            apiKeysObj.ClickOnApplicationEdit(tagName);

            ApplicationsWebhookPage applications = new ApplicationsWebhookPage(driver);

            applications.FilterDb("CreateApplicationWebhookTest");
            applications.CreateNewWebhook();

            applications.DeleteWebhook();

            //DELETE ASSERTION
            status = webhook.DeletionSuccess();
            Assert.True(status, "Applications Webhook was not deleted");
        }

        [Fact]
        [BeforeAfterTests]
        public void CreateChannelWebhookTest()
        {
            Db().Filter();
            LauchBrowser();
            GoTo("Login");

            LoginPage login = new LoginPage(driver);
            WebhookComponent webhook = new WebhookComponent(driver);

            login.UserCredentials();         

            homeObj = new HomeScreen(driver);
            homeObj.ClickOnApplication();

            //new AppcuesComponent(driver).CheckAppcuesNps();


            apiKeysObj = new APIKeys_ApplicationPage(driver);
            apiKeysObj.ClickOnNew();
            string tagName = Rand();
            apiKeysObj.EnterTagName(tagName);
            apiKeysObj.ClickOnOk();

            apiKeysObj.ClickOnApplicationEdit(tagName);
            editAppObj = new ChannelPage(driver);

            editAppObj.ClickOnNew();

            string ChannelName = Rand();
            editAppObj.EnterChannelID(ChannelName);
            editAppObj.ClickOnOk();

            //ChannelPage channel = new ApplicationsWebhookPage(driver).EditChannel();

            editChannelObj = new EditChannelPage(driver);

            editChannelObj.ClickOnChannel();

            editAppObj.FilterDb("CreateApplicationWebhookTest");
            editAppObj.CreateWebhook();

            
        }

        [Fact]
        [BeforeAfterTests]
        public void UpdateChannelWebhookTest()
        {
            Db().Filter();
            LauchBrowser();
            GoTo("Login");

            LoginPage login = new LoginPage(driver);
            WebhookComponent webhook = new WebhookComponent(driver);

            login.UserCredentials();


            homeObj = new HomeScreen(driver);
            homeObj.ClickOnApplication();

            //new AppcuesComponent(driver).CheckAppcuesNps();


            apiKeysObj = new APIKeys_ApplicationPage(driver);
            apiKeysObj.ClickOnNew();
            string tagName = Rand();
            apiKeysObj.EnterTagName(tagName);
            apiKeysObj.ClickOnOk();

            apiKeysObj.ClickOnApplicationEdit(tagName);
            editAppObj = new ChannelPage(driver);

            editAppObj.ClickOnNew();

            string ChannelName = Rand();
            editAppObj.EnterChannelID(ChannelName);
            editAppObj.ClickOnOk();

            //ChannelPage channel = new ApplicationsWebhookPage(driver).EditChannel();

            editChannelObj = new EditChannelPage(driver);

            editChannelObj.ClickOnChannel();

            editAppObj.FilterDb("CreateApplicationWebhookTest");
            editAppObj.CreateWebhook();

            editAppObj.FilterDb("UpdateChannelWebhookTest");
            editAppObj.UpdateWebhook();

            //UPDATE ASSERTION
            //status = webhook.UpdateSuccess();
            //Assert.True(status, "Channel Webhook was not able to save");
        }

        [Fact]
        [BeforeAfterTests]
        public void DeleteChannelWebhookTest()
        {
            Db().Filter();
            LauchBrowser();
            GoTo("Login");

            LoginPage login = new LoginPage(driver);
            WebhookComponent webhook = new WebhookComponent(driver);

            login.UserCredentials();

            homeObj = new HomeScreen(driver);
            homeObj.ClickOnApplication();

            //new AppcuesComponent(driver).CheckAppcuesNps();

            apiKeysObj = new APIKeys_ApplicationPage(driver);
            apiKeysObj.ClickOnNew();
            string tagName = Rand();
            apiKeysObj.EnterTagName(tagName);
            apiKeysObj.ClickOnOk();

            apiKeysObj.ClickOnApplicationEdit(tagName);
            editAppObj = new ChannelPage(driver);

            editAppObj.ClickOnNew();

            string ChannelName = Rand();
            editAppObj.EnterChannelID(ChannelName);
            editAppObj.ClickOnOk();

            editAppObj.FilterDb("CreateChannelWebhookTest");
            editAppObj.CreateWebhook();

            //channel.FilterDb("UpdateChannelWebhookTest");
            //channel.UpdateWebhook();

            editAppObj.DeleteWebhook();

            //DELETE ASSERTION
            status = webhook.DeletionSuccess();
            Assert.True(status, "Channel Webhook was not deleted");
        }
    }
}
