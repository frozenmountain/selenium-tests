﻿using System;
using System.Reflection;
using Xunit;

namespace LiveSwitchCloudConsole
{
    [Collection("Setup Collection")]

    public class SignupTests : TestBase 
    {      
        SignUpPage signupobj;
        HomeScreen homeobj;
        LandingPage landingPageObj;
        ConsentAgreement csa;
      
        [Fact]
        [BeforeAfterTests]
        public void Signup_FrozenMountain()
        {

            Db().Filter("FM", MethodBase.GetCurrentMethod().Name);
            LauchBrowser();
            GoTo("Signup");

            String Rng = Rand();

            landingPageObj = new LandingPage(driver);
            landingPageObj.ClickOnFreeTrial();

            signupobj = new SignUpPage(driver);
            signupobj.ClickOnCookies();
            signupobj.EnterUserName(NewUser(Rng));
            signupobj.EnterPassword(testDataService["Password"].ToString());
            signupobj.EnterFirstName(testDataService["Firstname"].ToString());
            signupobj.EnterLastName(testDataService["Lastname"].ToString());
            signupobj.EnterCompany(testDataService["Company"].ToString());
            signupobj.EnterPhoneNumber(testDataService["PhoneNumber"].ToString());
            signupobj.SelectCountry(testDataService["Country"].ToString());
            signupobj.SelectIndustry(testDataService["Industry"].ToString());
            signupobj.SelectRole(testDataService["Role"].ToString());
            signupobj.CheckPrivacyPolicy(testDataService["Consent"].ToString());
            signupobj.ClickSubmitButton();
            
            csa = new ConsentAgreement(driver);
            csa.Agreement();
            homeobj = new HomeScreen(driver);
           

            status = homeobj.GetMyAccounttxt();
            Assert.True(status);

        }

        [Fact]
        [BeforeAfterTests]
        public void Signup_FrozenMountain_NoConsent()
        {
            Db().Filter("FM", MethodBase.GetCurrentMethod().Name);
            LauchBrowser();
            GoTo("Signup");

            String Rng = Rand();
            landingPageObj = new LandingPage(driver);
            landingPageObj.ClickOnFreeTrial();
            signupobj = new SignUpPage(driver);
            signupobj.ClickOnCookies();
            signupobj.EnterUserName(NewUser(Rng));
            signupobj.EnterPassword(testDataService["Password"].ToString());
            signupobj.EnterFirstName(testDataService["Firstname"].ToString());
            signupobj.EnterLastName(testDataService["Lastname"].ToString());
            signupobj.EnterCompany(testDataService["Company"].ToString());
            signupobj.EnterPhoneNumber(testDataService["PhoneNumber"].ToString());
            signupobj.SelectCountry(testDataService["Country"].ToString());
            signupobj.SelectIndustry(testDataService["Industry"].ToString());
            signupobj.SelectRole(testDataService["Role"].ToString());
            signupobj.CheckPrivacyPolicy(testDataService["Consent"].ToString());
            signupobj.ClickSubmitButton();

            csa = new ConsentAgreement(driver);
            csa.Agreement();
            homeobj = new HomeScreen(driver);
           

            status = homeobj.GetMyAccounttxt();
            Assert.True(status);

        }

        [Fact]
        [BeforeAfterTests]
        public void Signup_FrozenMountain_Incong()
        {

            Db().Filter("FM", MethodBase.GetCurrentMethod().Name);
            LauchBrowser();
            GoTo("Signup");

            String Rng = Rand();
            landingPageObj = new LandingPage(driver);
            landingPageObj.ClickOnFreeTrial();
            signupobj = new SignUpPage(driver);
            signupobj.ClickOnCookies();
            signupobj.EnterUserName(NewUser(Rng));
            signupobj.EnterPassword(testDataService["Password"].ToString());
            signupobj.EnterFirstName(testDataService["Firstname"].ToString());
            signupobj.EnterLastName(testDataService["Lastname"].ToString());
            signupobj.EnterCompany(testDataService["Company"].ToString());
            signupobj.EnterPhoneNumber(testDataService["PhoneNumber"].ToString());
            signupobj.SelectCountry(testDataService["Country"].ToString());
            signupobj.SelectIndustry(testDataService["Industry"].ToString());
            signupobj.SelectRole(testDataService["Role"].ToString());
            signupobj.CheckPrivacyPolicy(testDataService["Consent"].ToString());
            signupobj.ClickSubmitButton();

            csa = new ConsentAgreement(driver);
            csa.Agreement();
            homeobj = new HomeScreen(driver);
            

            status = homeobj.GetMyAccounttxt();
            Assert.True(status);

        }

        [Fact]
        [BeforeAfterTests]
        public void Signup_FrozenMountain_Incong_NoConsent()
        {

            Db().Filter("FM", MethodBase.GetCurrentMethod().Name);
            LauchBrowser();
            GoTo("Signup");

            String Rng = Rand();
            landingPageObj = new LandingPage(driver);
            landingPageObj.ClickOnFreeTrial();
            signupobj = new SignUpPage(driver);
            signupobj.ClickOnCookies();
            signupobj.EnterUserName(NewUser(Rng));
            signupobj.EnterPassword(testDataService["Password"].ToString());
            signupobj.EnterFirstName(testDataService["Firstname"].ToString());
            signupobj.EnterLastName(testDataService["Lastname"].ToString());
            signupobj.EnterCompany(testDataService["Company"].ToString());
            signupobj.EnterPhoneNumber(testDataService["PhoneNumber"].ToString());
            signupobj.SelectCountry(testDataService["Country"].ToString());
            signupobj.SelectIndustry(testDataService["Industry"].ToString());
            signupobj.SelectRole(testDataService["Role"].ToString());
            signupobj.CheckPrivacyPolicy(testDataService["Consent"].ToString());
            signupobj.ClickSubmitButton();

            csa = new ConsentAgreement(driver);
            csa.Agreement();
            homeobj = new HomeScreen(driver);
           

            status = homeobj.GetMyAccounttxt();
            Assert.True(status);

        }


        [Fact]
        [BeforeAfterTests]
        public void Signup_Liveswitch()
        {
            Db().Filter(MethodBase.GetCurrentMethod().Name);
            LauchBrowser();
            GoTo("Signup");

            String Rng = Rand();

            if(testDataService["Environment"].ToString().Equals("Videolanding"))
            {

                landingPageObj = new LandingPage(driver);
                landingPageObj.Signup_Environment();
            }
            if (testDataService["Environment"].ToString().Equals("liveswitch"))
            {

                landingPageObj = new LandingPage(driver);
                landingPageObj.Sign_up_FM();
            }

            signupobj = new SignUpPage(driver);
            signupobj.ClickOnCookies();
            signupobj.EnterUserName(NewUser(Rng));
            signupobj.EnterPassword(testDataService["Password"].ToString());
            signupobj.EnterFirstName(testDataService["Firstname"].ToString());
            signupobj.EnterLastName(testDataService["Lastname"].ToString());
            signupobj.EnterCompany(testDataService["Company"].ToString());
            signupobj.EnterPhoneNumber(testDataService["PhoneNumber"].ToString());
            signupobj.SelectCountry(testDataService["Country"].ToString());
            signupobj.CheckPrivacyPolicy(testDataService["Consent"].ToString());
            signupobj.ClickSubmitButton();

            csa = new ConsentAgreement(driver);
            csa.Agreement();
            homeobj = new HomeScreen(driver);
            homeobj.SwitchToFrame();
            homeobj.ClearCookies();

            status = homeobj.GetLetGetStarted();
            Assert.True(status);

        }


        [Fact]
        [BeforeAfterTests]
        public void Signup_Liveswitch_NoConsent()
        {
            Db().Filter(MethodBase.GetCurrentMethod().Name);
            LauchBrowser();
            GoTo("Signup");

            String Rng = Rand();
            if (testDataService["Environment"].ToString().Equals("Videolanding"))
            {

                landingPageObj = new LandingPage(driver);
                landingPageObj.Signup_Environment();
            }
            if (testDataService["Environment"].ToString().Equals("liveswitch"))
            {

                landingPageObj = new LandingPage(driver);
                landingPageObj.Sign_up_FM();
            }

            signupobj = new SignUpPage(driver);
            signupobj.ClickOnCookies();
            signupobj.EnterUserName(NewUser(Rng));
            signupobj.EnterPassword(testDataService["Password"].ToString());
            signupobj.EnterFirstName(testDataService["Firstname"].ToString());
            signupobj.EnterLastName(testDataService["Lastname"].ToString());
            signupobj.EnterCompany(testDataService["Company"].ToString());
            signupobj.EnterPhoneNumber(testDataService["PhoneNumber"].ToString());
            signupobj.SelectCountry(testDataService["Country"].ToString());
            signupobj.CheckPrivacyPolicy(testDataService["Consent"].ToString());
            signupobj.ClickSubmitButton();

            csa = new ConsentAgreement(driver);
            csa.Agreement();
            homeobj = new HomeScreen(driver);

            homeobj.SwitchToFrame();
            homeobj.ClearCookies();

            status = homeobj.GetLetGetStarted();
            Assert.True(status);
        }

        [Fact]
        [BeforeAfterTests]
        public void Signup_Liveswitch_lncog()
        {
            Db().Filter(MethodBase.GetCurrentMethod().Name);
            LauchBrowser(TestBase.testDataService["incognito"].ToString());
            GoTo("Signup");

            String Rng = Rand();

            if (testDataService["Environment"].ToString().Equals("Videolanding"))
            {

                landingPageObj = new LandingPage(driver);
                landingPageObj.Signup_Environment();
            }
            if (testDataService["Environment"].ToString().Equals("liveswitch"))
            {

                landingPageObj = new LandingPage(driver);
                landingPageObj.Sign_up_FM();
            }

            signupobj = new SignUpPage(driver);
            signupobj.ClickOnCookies();
            signupobj.EnterUserName(NewUser(Rng));
            signupobj.EnterPassword(testDataService["Password"].ToString());
            signupobj.EnterFirstName(testDataService["Firstname"].ToString());
            signupobj.EnterLastName(testDataService["Lastname"].ToString());
            signupobj.EnterCompany(testDataService["Company"].ToString());
            signupobj.EnterPhoneNumber(testDataService["PhoneNumber"].ToString());
            signupobj.SelectCountry(testDataService["Country"].ToString());
            signupobj.CheckPrivacyPolicy(testDataService["Consent"].ToString());
            signupobj.ClickSubmitButton();

            csa = new ConsentAgreement(driver);
            csa.Agreement();
            homeobj = new HomeScreen(driver);

            homeobj.SwitchToFrame();
            homeobj.ClearCookies();

            status = homeobj.GetLetGetStarted();
            Assert.True(status);

        }

        [Fact]
        [BeforeAfterTests]
        public void Signup_Liveswitch_lncog_NoConsent()
        {

            Db().Filter(MethodBase.GetCurrentMethod().Name);
            LauchBrowser(TestBase.testDataService["incognito"].ToString());
            GoTo("Signup");

            String Rng = Rand();

            if (testDataService["Environment"].ToString().Equals("Videolanding"))
            {

                landingPageObj = new LandingPage(driver);
                landingPageObj.Signup_Environment();
            }
            if (testDataService["Environment"].ToString().Equals("liveswitch"))
            {

                landingPageObj = new LandingPage(driver);
                landingPageObj.Sign_up_FM();
            }

            signupobj = new SignUpPage(driver);
            signupobj.ClickOnCookies();
            signupobj.EnterUserName(NewUser(Rng));
            signupobj.EnterPassword(testDataService["Password"].ToString());
            signupobj.EnterFirstName(testDataService["Firstname"].ToString());
            signupobj.EnterLastName(testDataService["Lastname"].ToString());
            signupobj.EnterCompany(testDataService["Company"].ToString());
            signupobj.EnterPhoneNumber(testDataService["PhoneNumber"].ToString());
            signupobj.SelectCountry(testDataService["Country"].ToString());
            signupobj.CheckPrivacyPolicy(testDataService["Consent"].ToString());
            signupobj.ClickSubmitButton();

            csa = new ConsentAgreement(driver);
            csa.Agreement();
            homeobj = new HomeScreen(driver);

            homeobj.SwitchToFrame();
            homeobj.ClearCookies();

            status = homeobj.GetLetGetStarted();
            Assert.True(status);

        }

        [Fact]
        [BeforeAfterTests]
        public void Signup_FrozenMountain_FM2Fdown()
        {

            Db().Filter("FM2Fdown", MethodBase.GetCurrentMethod().Name);
            LauchBrowser();
            GoTo("Signup");

            String Rng = Rand();

            signupobj = new SignUpPage(driver);
            signupobj.ClickOnCookies();
            signupobj.EnterUserName(NewUser(Rng));
            signupobj.EnterPassword(testDataService["Password"].ToString());
            signupobj.EnterFirstName(testDataService["Firstname"].ToString());
            signupobj.EnterLastName(testDataService["Lastname"].ToString());
            signupobj.EnterCompany(testDataService["Company"].ToString());
            signupobj.EnterPhoneNumber(testDataService["PhoneNumber"].ToString());
            signupobj.SelectCountry(testDataService["Country"].ToString());
            signupobj.SelectIndustry(testDataService["Industry"].ToString());
            signupobj.SelectRole(testDataService["Role"].ToString());
            signupobj.CheckPrivacyPolicy(testDataService["Consent"].ToString());
            signupobj.ClickSubmitButton();

            csa = new ConsentAgreement(driver);
            csa.Agreement();
            homeobj = new HomeScreen(driver);


            status = homeobj.GetMyAccounttxtDownload();
            Assert.True(status);

        }


        [Fact]
        [BeforeAfterTests]
        public void Signup_FrozenMountain_FM2FPurch()
        {

            Db().Filter("FM2FPurch", MethodBase.GetCurrentMethod().Name);
            LauchBrowser();
            GoTo("Signup");

            String Rng = Rand();
            signupobj = new SignUpPage(driver);
            signupobj.ClickOnCookies();
            signupobj.EnterUserName(NewUser(Rng));
            signupobj.EnterPassword(testDataService["Password"].ToString());
            signupobj.EnterFirstName(testDataService["Firstname"].ToString());
            signupobj.EnterLastName(testDataService["Lastname"].ToString());
            signupobj.EnterCompany(testDataService["Company"].ToString());
            signupobj.EnterPhoneNumber(testDataService["PhoneNumber"].ToString());
            signupobj.SelectCountry(testDataService["Country"].ToString());
            signupobj.SelectIndustry(testDataService["Industry"].ToString());
            signupobj.SelectRole(testDataService["Role"].ToString());
            signupobj.CheckPrivacyPolicy(testDataService["Consent"].ToString());
            signupobj.ClickSubmitButton();

            csa = new ConsentAgreement(driver);
            csa.Agreement();
            homeobj = new HomeScreen(driver);

            status = homeobj.GetMyAccounttxt();
            Assert.True(status);

        }

        [Fact]
        [BeforeAfterTests]
        public void Signup_FrozenMountain_FM2Fdownreturn()
        {

            Db().Filter("FM2Fdownreturn", MethodBase.GetCurrentMethod().Name);
            LauchBrowser();
            GoTo("Signup");

            String Rng = Rand();
            signupobj = new SignUpPage(driver);
            signupobj.ClickOnCookies();
            signupobj.EnterUserName(NewUser(Rng));
            signupobj.EnterPassword(testDataService["Password"].ToString());
            signupobj.EnterFirstName(testDataService["Firstname"].ToString());
            signupobj.EnterLastName(testDataService["Lastname"].ToString());
            signupobj.EnterCompany(testDataService["Company"].ToString());
            signupobj.EnterPhoneNumber(testDataService["PhoneNumber"].ToString());
            signupobj.SelectCountry(testDataService["Country"].ToString());
            signupobj.SelectIndustry(testDataService["Industry"].ToString());
            signupobj.SelectRole(testDataService["Role"].ToString());
            signupobj.CheckPrivacyPolicy(testDataService["Consent"].ToString());
            signupobj.ClickSubmitButton();

            csa = new ConsentAgreement(driver);
            csa.Agreement();
            homeobj = new HomeScreen(driver);


            status = homeobj.GetMyAccounttxtDownload();
            Assert.True(status);

        }

        [Fact]
        [BeforeAfterTests]
        public void Signup_FrozenMountain_FM2Fpurchreturn()
        {

            Db().Filter("FM2Fpurchreturn", MethodBase.GetCurrentMethod().Name);
            LauchBrowser();
            GoTo("Signup");

            String Rng = Rand();
            signupobj = new SignUpPage(driver);
            signupobj.ClickOnCookies();
            signupobj.EnterUserName(NewUser(Rng));
            signupobj.EnterPassword(testDataService["Password"].ToString());
            signupobj.EnterFirstName(testDataService["Firstname"].ToString());
            signupobj.EnterLastName(testDataService["Lastname"].ToString());
            signupobj.EnterCompany(testDataService["Company"].ToString());
            signupobj.EnterPhoneNumber(testDataService["PhoneNumber"].ToString());
            signupobj.SelectCountry(testDataService["Country"].ToString());
            signupobj.SelectIndustry(testDataService["Industry"].ToString());
            signupobj.SelectRole(testDataService["Role"].ToString());
            signupobj.CheckPrivacyPolicy(testDataService["Consent"].ToString());
            signupobj.ClickSubmitButton();

            csa = new ConsentAgreement(driver);
            csa.Agreement();
            homeobj = new HomeScreen(driver);


            status = homeobj.GetMyAccounttxt();
            Assert.True(status);

        }
    }
}
