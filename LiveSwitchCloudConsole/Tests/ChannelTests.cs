﻿
using System.Reflection;
using Xunit;

namespace LiveSwitchCloudConsole
{
    [Collection("Setup Collection")]

    public class ChannelTests : TestBase
    {
        LoginPage loginObj;
        HomeScreen homeObj;
        APIKeys_ApplicationPage apiKeysObj;
        ChannelPage editAppObj;
        EditChannelPage editChannelObj;
        AppcuesComponent appcuesComponentObj;

        [Fact]
        [BeforeAfterTests]
        public void Channel_Add()
        {
            Db().Filter(MethodBase.GetCurrentMethod().Name);
            LauchBrowser();
            GoTo("Login");

            loginObj = new LoginPage(driver);
            loginObj.UserCredentials();       

            homeObj = new HomeScreen(driver);

            homeObj.ClickOnApplication();

            apiKeysObj = new APIKeys_ApplicationPage(driver);
            apiKeysObj.ClickOnNew();
            string tagName = Rand();
            apiKeysObj.EnterTagName(tagName);
            apiKeysObj.ClickOnOk();

            apiKeysObj.ClickOnApplicationEdit(tagName);

            editAppObj = new ChannelPage(driver);
           
            editAppObj.ClickOnNew();
            
            string ChannelName = Rand();
            editAppObj.EnterChannelID(ChannelName);
            editAppObj.ClickOnOk();
            status = editAppObj.ValidateChannelData(ChannelName);
            Assert.True(status);

        }


        [Fact]
        [BeforeAfterTests]
        public void Channel_Delete()
        {
            Db().Filter(MethodBase.GetCurrentMethod().Name);
            LauchBrowser();
            GoTo("Login");

            loginObj = new LoginPage(driver);
            loginObj.UserCredentials();
                      

            homeObj = new HomeScreen(driver);
            homeObj.ClickOnApplication();

            apiKeysObj = new APIKeys_ApplicationPage(driver);
            apiKeysObj.ClickOnNew();
            string tagName = Rand();
            apiKeysObj.EnterTagName(tagName);
            apiKeysObj.ClickOnOk();

            apiKeysObj.ClickOnApplicationEdit(tagName);

            editAppObj = new ChannelPage(driver);
            editAppObj.ClickOnNew();
            string ChannelName = Rand();
            editAppObj.EnterChannelID(ChannelName);
            editAppObj.ClickOnOk();
            status = editAppObj.DeleteChannelData(ChannelName);

            Assert.True(status);

        }


        [Fact]
        [BeforeAfterTests]
        public void CanUpdateAudioCodecs()
        {
            Db().Filter(MethodBase.GetCurrentMethod().Name);
            LauchBrowser();
            GoTo("Login");

            loginObj = new LoginPage(driver);
            loginObj.UserCredentials();

            homeObj = new HomeScreen(driver);
            homeObj.ClickOnApplication();

            apiKeysObj = new APIKeys_ApplicationPage(driver);
            apiKeysObj.ClickOnNew();
            string tagName = Rand();
            apiKeysObj.EnterTagName(tagName);
            apiKeysObj.ClickOnOk();

            apiKeysObj.ClickOnApplicationEdit(tagName);

            editAppObj = new ChannelPage(driver);
            editAppObj.ClickOnNew();
            string ChannelName = Rand();
            editAppObj.EnterChannelID(ChannelName);
            editAppObj.ClickOnOk();

            editChannelObj = new EditChannelPage(driver);

            editChannelObj.ClickOnChannel();

            //This test should include toggle functions and max bitrate validations
            editChannelObj.UpdateAudioCodec();

            status = editChannelObj.UpdateSuccessMsgDisplayed();

            Assert.True(status);

            //TODO: Figure out a way to validate that all toggles have been checked properly after submit
        }

        [Fact]
        [BeforeAfterTests]
        public void CanUpdateVideoCodecs()
        {
            Db().Filter(MethodBase.GetCurrentMethod().Name);
            LauchBrowser();
            GoTo("Login");

            loginObj = new LoginPage(driver);
            loginObj.UserCredentials();

            homeObj = new HomeScreen(driver);
            homeObj.ClickOnApplication();

            apiKeysObj = new APIKeys_ApplicationPage(driver);
            apiKeysObj.ClickOnNew();
            string tagName = Rand();
            apiKeysObj.EnterTagName(tagName);
            apiKeysObj.ClickOnOk();

            apiKeysObj.ClickOnApplicationEdit(tagName);
            editAppObj = new ChannelPage(driver);
            editAppObj.ClickOnNew();
            string ChannelName = Rand();
            editAppObj.EnterChannelID(ChannelName);
            editAppObj.ClickOnOk();

            editChannelObj = new EditChannelPage(driver);

            editChannelObj.ClickOnChannel();

            //This test should include toggle functions and max bitrate validations
            editChannelObj.UpdateVideoCodec();

            status = editChannelObj.UpdateSuccessMsgDisplayed();

            Assert.True(status);
        }

        [Fact]
        [BeforeAfterTests]
        public void CanSetMcuConfigs()
        {
            Db().Filter(MethodBase.GetCurrentMethod().Name);
            LauchBrowser();
            GoTo("Login");

            loginObj = new LoginPage(driver);
            loginObj.UserCredentials();

            homeObj = new HomeScreen(driver);
            homeObj.ClickOnApplication();

            apiKeysObj = new APIKeys_ApplicationPage(driver);
            apiKeysObj.ClickOnNew();
            string tagName = Rand();
            apiKeysObj.EnterTagName(tagName);
            apiKeysObj.ClickOnOk();

            apiKeysObj.ClickOnApplicationEdit(tagName);

            editAppObj = new ChannelPage(driver);
            editAppObj.ClickOnNew();
            string ChannelName = Rand();
            editAppObj.EnterChannelID(ChannelName);

            editAppObj.ClickOnOk();

            editChannelObj = new EditChannelPage(driver);

            editChannelObj.ClickOnChannel();

            editChannelObj.SetMcuConfig();

            status = editChannelObj.UpdateSuccessMsgDisplayed();

            Assert.True(status);
        }

        //TODO: Add  MCU Simulcast Config in a separate test

        [Fact]
        [BeforeAfterTests]
        public void CanSetSfuConfigs()
        {
            Db().Filter(MethodBase.GetCurrentMethod().Name);
            LauchBrowser();
            GoTo("Login");

            loginObj = new LoginPage(driver);
            loginObj.UserCredentials();

            homeObj = new HomeScreen(driver);
            homeObj.ClickOnApplication();
            
            apiKeysObj = new APIKeys_ApplicationPage(driver);
            apiKeysObj.ClickOnNew();
            string tagName = Rand();
            apiKeysObj.EnterTagName(tagName);
            apiKeysObj.ClickOnOk();

            apiKeysObj.ClickOnApplicationEdit(tagName);

            editAppObj = new ChannelPage(driver);
            editAppObj.ClickOnNew();
            string ChannelName = Rand();
            editAppObj.EnterChannelID(ChannelName);

            editAppObj.ClickOnOk();

            editChannelObj = new EditChannelPage(driver);
            editChannelObj.ClickOnChannel();

            editChannelObj.SetSfuConfig();

            status = editChannelObj.UpdateSuccessMsgDisplayed();

            Assert.True(status);
        }

        //TODO: Add  Sfu Simulcast Config in a separate test
    }
}
