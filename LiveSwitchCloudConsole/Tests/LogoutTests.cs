﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Xunit;

namespace LiveSwitchCloudConsole
{
    [Collection("Setup Collection")]

    public class LogoutTests : TestBase
    {
        LoginPage loginObj;
        HomeScreen homeObj;
        LandingPage landingPageObj;

        [Fact]
        [BeforeAfterTests]
        public void Logout()
        {
            Db().Filter(MethodBase.GetCurrentMethod().Name);
            LauchBrowser();
            GoTo("Login");

            loginObj = new LoginPage(driver);
            loginObj.UserCredentials();

            homeObj = new HomeScreen(driver);
            homeObj.FrozenMountainDropDown();
            homeObj.LogoutButton_Environment();

            status = loginObj.IsLoginButtonDisplayed();

            Assert.True(status);

        }

        
        [Fact]
        [BeforeAfterTests]
        public void Logout_FM()
        {
            Db().Filter("FM", MethodBase.GetCurrentMethod().Name);
            LauchBrowser();
            GoTo("Login");

            loginObj = new LoginPage(driver);
            loginObj.UserCredentials();

            homeObj = new HomeScreen(driver);
        
            homeObj.LogoutButton_FM();
            
            landingPageObj = new LandingPage(driver);

            status = landingPageObj.IsFreeTrailButtonDisplayed();
            Assert.True(status);

        }

    }
}
