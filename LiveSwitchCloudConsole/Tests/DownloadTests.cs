﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Xunit;

namespace LiveSwitchCloudConsole
{
    [Collection("Setup Collection")]

    public class DownloadTests : TestBase
    {
        HomeScreen homeObj;
        LoginPage login;
        DownloadsPage downloads;

        [Fact]
        [BeforeAfterTests]

        public void DownloadAlpha()
        {
            Db().Filter(MethodBase.GetCurrentMethod().Name);
            LauchBrowser();
            GoTo("Login");

            login = new LoginPage(driver);
            login.UserCredentials();

            homeObj = new HomeScreen(driver);
            homeObj.ClickOnDownloads();
            downloads = new DownloadsPage(driver);

            downloads.ClickOnAlphaDownloads();


        }

        [Fact]
        [BeforeAfterTests]
        public void DownloadBeta()
        {
            Db().Filter(MethodBase.GetCurrentMethod().Name);
            LauchBrowser();
            GoTo("Login");

            login = new LoginPage(driver);
            login.UserCredentials();
            homeObj = new HomeScreen(driver);
            homeObj.ClickOnDownloads();
            downloads.ClickOnBetaDownloads();

        }
    }
}