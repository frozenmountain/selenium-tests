﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace LiveSwitchCloudConsole
{
    public class AppcuesComponent : BasePage
    {
        protected IWebDriver driver;
        private static readonly By GettingStartedBtn = By.CssSelector("a[data-step='end']");
        private static readonly By GettingStartedBtn1 = By.XPath("//div[@class='checklist']/div[1]");
        private static readonly By AppcuesChecklist = By.CssSelector("div[class='checklist-main expanded']");

        private static readonly By AppcuesNpsCloseBtn = By.Id("ask-me-later");

        public AppcuesComponent(IWebDriver driver) : base(driver)
        {
            this.driver = driver;
        }

        public void CloseModal()
        {
            SwitchToTopFrame();
            Click(GettingStartedBtn);
        }
        public void CloseAppecues()
        {
            SwitchToTopFrame();
            Click(GettingStartedBtn1);
            SwitchToParentFrame();
        }

        public bool ChecklistIsDisplayed()
        {
            SwitchToTopFrame();
            return wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(AppcuesChecklist)).Displayed;
        }

        public void CheckAppcuesNps()
        {
            //Handles the appcues rating modal
            //IWebElement AppcuesNpsContainer = wait.Until(ExpectedConditions.ElementExists(AppcuesNps));
            try
            {
                SwitchToTopFrame();
                try {
                    JsExecuteClick(AppcuesNpsCloseBtn);
                }
                catch(WebDriverException)
                {
                    driver.SwitchTo().DefaultContent();
                }
            }
            catch (Exception)
            {
                ; //do nothing
            }
        }

    }
}