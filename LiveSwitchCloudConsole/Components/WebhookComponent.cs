﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Xunit;

namespace LiveSwitchCloudConsole
{
    public class WebhookComponent : BasePage
    {
        protected IWebDriver driver;

        private static readonly By CreateWebhookBtn = By.XPath("//button[@data-tour='webhook_new']");



        private static readonly By WebhookNameInput = By.XPath("//div[@data-tour='webhook_modal']//input[@placeholder='Name']");

        private static readonly By WebhookUrlInput = By.XPath("//div[@data-tour='webhook_modal']//input[@placeholder='Url']");     

        private static readonly By WebhookApplicationCreateBtn = By.XPath("//div[@class='modal-footer']/button[1]");

        private static readonly By WebhookChannelCreateBtn = By.XPath("//div[@class='md-ripple']//div[contains(@class, 'md-button-content') and contains(text(), 'Create')]");

       

        private static readonly By WebhookOKBtn = By.XPath("//div[contains(@class, 'md-button-content') and contains(text(), 'OK')]");

        private static readonly By WebhookApplyBtn = By.XPath("//div[contains(@class, 'md-button-content') and contains(text(), 'Apply')]");

        private static readonly string WebhookOption = "//div[@data-tour='webhook_modal']//label[contains(text(), '{0}')]";

        private static readonly By WebhookDeleteBtn = By.XPath("//td[@class='md-table-cell actions webhook actions']//i[contains(text(), 'close')]");

        private static readonly By WebhookConfirmDeleteBtn = By.XPath("//button[contains(text(), 'Yes, delete it!')]");

        private static readonly By WebhookNameDiv = By.XPath("//tr[@class='md-table-row webhooksListingRow']/td[1]/div[1]");

        private static readonly By UpdateSuccessMsg = By.XPath("//div[@class='md-snackbar-content']//span[contains(text(), 'Your changes have been saved.')]");

        private static readonly By WebhookConfirmedDeletionModal = By.XPath("//h2[contains(text(), 'Deleted!')]");

        string WebhookID = "//div[@data-tour='application_card_webhooks']//table//tr//td[1]//div";


        public WebhookComponent(IWebDriver driver) : base(driver)
        {
            this.driver = driver;
        }

        public void WebhookCreate()
        {
            JsExecuteClick(CreateWebhookBtn);
            
            string WebhookName = Rand();

            Type(WebhookNameInput, WebhookName);

            Type(WebhookUrlInput, Db().GetField("WebhookURL"));

            SelectOptions(Db().GetField("WebhookEvent"));
            SelectOptions(Db().GetField("WebhookEventOptions"));
            SelectOptions(Db().GetField("WebhookAdvancedOptions"));
            //driver.FindElement(WebhookApplicationCreateBtn).Click();
            
            JsExecuteClick(WebhookApplicationCreateBtn);

            //Actions actions = new Actions(driver);

            //IWebElement submit= driver.FindElement(WebhookApplicationCreateBtn);

            //actions.MoveToElement(submit).Perform();
            
            Assert.True(ValidateWebhook(WebhookName));

        }

        

        public void Update()
        {
            //JsExecuteClick(WebhookNameDiv);
            driver.FindElement(WebhookNameDiv).Click();
            Clear(WebhookNameInput);
            //Clear(WebhookUrlInput);
            //Type(WebhookNameInput, Db().GetField("WebhookName"));

            string WebhookName = Rand();

            Type(WebhookNameInput, WebhookName);
            //Type(WebhookUrlInput, Db().GetField("WebhookURL"));

            //SelectOptions(Db().GetField("WebhookEvent"));
            //JsExecuteClick(WebhookOKBtn);
            //SelectOptions(Db().GetField("WebhookEventOptions"));
            //SelectOptions(Db().GetField("WebhookAdvancedOptions"));

            JsExecuteClick(WebhookApplicationCreateBtn);
            Assert.True(ValidateWebhook(WebhookName));
        }

        public void Delete()
        {
            JsExecuteClick(WebhookDeleteBtn);
            JsExecuteClick(WebhookConfirmDeleteBtn);
        }

        private void SelectOptions(string val)
        {
            var options = val.Split(',').Select(p => p.Trim());
            foreach (var option in options)
            {
                JsExecuteClick(By.XPath(string.Format(WebhookOption, option)));
            }
        }

        public bool CreationSuccess()
        {
            return wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(WebhookNameDiv)).Displayed;
        }

        public bool UpdateSuccess()
        {
            return wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(UpdateSuccessMsg)).Displayed;
        }

        public bool DeletionSuccess()
        {
            return wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(WebhookConfirmedDeletionModal)).Displayed;
        }


        public bool ValidateWebhook(string keyName)
        {
            Thread.Sleep(3000);

            bool status = false;
            IList<IWebElement> ele = driver.FindElements(By.XPath(WebhookID));

            foreach (var i in ele)
            {
                string value = i.Text;
                //string value = val.Split("\n")[1];

                if (keyName.Equals(value))
                {

                    status = true;
                    break;
                }

            }


            return status;
        }
    }
}
