﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace LiveSwitchCloudConsole
{
    class BillingPage : TestBase
    {        
        IWebElement InvoiceOragnizationName => driver.FindElement(By.XPath("//input[@data-tour='organization_name']"));

        IWebElement InvoiceAddress1 => driver.FindElement(By.XPath("//h4[normalize-space()='Invoicing Details']/parent::div/parent::div//input[@data-tour='address_1']"));
        IWebElement InvoiceAddress2 => driver.FindElement(By.XPath("//h4[normalize-space()='Invoicing Details']/parent::div/parent::div//input[@data-tour='address_2']"));

        IWebElement InvoiceCountry => driver.FindElement(By.XPath("//select[@data-tour='address_city']"));


        IWebElement InvoiceState => driver.FindElement(By.XPath("//h4[normalize-space()='Invoicing Details']/parent::div/parent::div//select[@data-tour='address_state']"));

        IWebElement InvoiceCity => driver.FindElement(By.XPath("//h4[normalize-space()='Invoicing Details']/parent::div/parent::div//input[@data-tour='address_city']"));

        IWebElement InvoicePostalCode => driver.FindElement(By.XPath("//h4[normalize-space()='Invoicing Details']/parent::div/parent::div//input[@data-tour='address_postalcode']"));


        IWebElement PaymentDetailsFirstName => driver.FindElement(By.XPath("//input[@data-tour='first_name_on_card']"));


        IWebElement PaymentDetailsLasName => driver.FindElement(By.XPath("//input[@data-tour='last_name_on_card']"));

        IWebElement PaymentDetailsCardNumber => driver.FindElement(By.XPath("//input[@data-tour='card_number']"));


        IWebElement PaymentDetailsExpiryMonth => driver.FindElement(By.XPath("//input[@data-tour='expiry_month']"));

        IWebElement PaymentDetailsExpiryYear => driver.FindElement(By.XPath("//input[@data-tour='expiry_year']"));

        IWebElement PaymentDetailsSecurityNumber => driver.FindElement(By.XPath("//input[@data-tour='security_number']"));

        IWebElement PaymentAddress_Address1 => driver.FindElement(By.XPath("//h4[normalize-space()='Payment Address Details']/parent::div/parent::div//input[@data-tour='address_1']"));

        IWebElement PaymentAddress_Address2 => driver.FindElement(By.XPath("//h4[normalize-space()='Payment Address Details']/parent::div/parent::div//input[@data-tour='address_2']"));

        IWebElement PaymentAddress_Country => driver.FindElement(By.XPath("//select[@data-tour='address_country']"));

        IWebElement PaymentAddress_State => driver.FindElement(By.XPath("//h4[normalize-space()='Payment Address Details']/parent::div/parent::div//select[@data-tour='address_state']"));

        IWebElement PaymentAddress_City => driver.FindElement(By.XPath("//h4[normalize-space()='Payment Address Details']/parent::div/parent::div//input[@data-tour='address_city']"));

        IWebElement PaymentAddress_PostalCode => driver.FindElement(By.XPath("//h4[normalize-space()='Payment Address Details']/parent::div/parent::div//input[@data-tour='address_postalcode']"));

        IWebElement UpdateSuccessMsg => driver.FindElement(By.XPath("//div[@class='md-snackbar-content']//span[contains(text(), 'Your changes have been saved.')]"));

        IWebElement BtnSubmit => driver.FindElement(By.XPath("//button[@id='snackbar-submit']"));


       


        public BillingPage(IWebDriver driver)
        {
            this.driver = driver;

        }

        public void UpdateOrganization(string name)
        {
            InvoiceOragnizationName.Clear();
            InvoiceOragnizationName.SendKeys(name);

        }
        public void UpdateInvoiceAddress1(string address1)
        {
            InvoiceAddress1.Clear();
            InvoiceAddress1.SendKeys(address1);

        }
        public void UpdateInvoiceAddress2(string address2)
        {
            InvoiceAddress2.Clear();
            InvoiceAddress2.SendKeys(address2);

        }
        public void UpdateInvoiceCountry(string countryName)
        {
            SelectElement ele = new SelectElement(InvoiceCountry);
            ele.SelectByText(countryName);
        }
        public void UpdateInvoiceState(string stateName)
        {
            SelectElement ele = new SelectElement(InvoiceState);
            ele.SelectByText(stateName);
        }
        public void UpdateInvoiceCity(string cityName)
        {
            InvoiceCity.Clear();
            InvoiceCity.SendKeys(cityName);

        }
        public void UpdateInvoicePostalCode(string postalCode)
        {
            InvoicePostalCode.Clear();
            InvoicePostalCode.SendKeys(postalCode);

        }


        public void ClickOnSubmit()
        {
            Thread.Sleep(3000);
            BtnSubmit.Click();
        }


        public bool UpdateMessageDisplay()
        {
            return UpdateSuccessMsg.Displayed;
        }

        public void UpdatePaymentDetailsFirstName(string firstName)
        {
            PaymentDetailsFirstName.Clear();
            PaymentDetailsFirstName.SendKeys(firstName);

        }

        public void UpdatePaymentDetailLastName(string lastName)
        {
            PaymentDetailsLasName.Clear();
            PaymentDetailsLasName.SendKeys(lastName);

        }

        public void UpdatePaymentDetailsCardNumber(string cardNumber)
        {
            PaymentDetailsCardNumber.Clear();
            PaymentDetailsCardNumber.SendKeys(cardNumber);
        }


        public void UpdatePaymentDetailsExpiryMonth(string month)
        {
            PaymentDetailsExpiryMonth.Clear();
            PaymentDetailsExpiryMonth.SendKeys(month);
        }


        public void UpdatePaymentDetailsExpiryYear(string year)
        {
            PaymentDetailsExpiryYear.Clear();
            PaymentDetailsExpiryYear.SendKeys(year);
        }

        public void UpdatePaymentDetailsExpiryCvv(string cvv)
        {
            PaymentDetailsSecurityNumber.Clear();
            PaymentDetailsSecurityNumber.SendKeys(cvv);
        }

        public void UpdatePaymentAddress1(string address1)
        {
            PaymentAddress_Address1.Clear();
            PaymentAddress_Address1.SendKeys(address1);

        }
        public void UpdatePaymentAddress2(string address2)
        {
            PaymentAddress_Address2.Clear();
            PaymentAddress_Address2.SendKeys(address2);

        }
        public void UpdatePaymentCountry(string countryName)
        {
            SelectElement ele = new SelectElement(PaymentAddress_Country);
            ele.SelectByText(countryName);
        }
        public void UpdatePaymentState(string stateName)
        {
            SelectElement ele = new SelectElement(PaymentAddress_State);
            ele.SelectByText(stateName);
        }
        public void UpdatePaymentCity(string cityName)
        {
            PaymentAddress_City.Clear();
            PaymentAddress_City.SendKeys(cityName);

        }
        public void UpdatePaymentPostalCode(string postalCode)
        {
            PaymentAddress_PostalCode.Clear();
            PaymentAddress_PostalCode.SendKeys(postalCode);

        }

        public void scrollDown()
        {
            Actions actions = new Actions(driver);
            actions.MoveToElement(PaymentAddress_PostalCode);
            actions.Perform();


        }



    }
}
