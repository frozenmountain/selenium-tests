﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using AventStack.ExtentReports.Utils;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;

namespace LiveSwitchCloudConsole
{
    class EditChannelPage : BasePage
    {
        IWebDriver driver;

        IWebElement BtnSubmit => driver.FindElement(By.XPath("//button[@data-tour='submit']"));

        IWebElement UpdateSuccessMsg => driver.FindElement(By.XPath("//div[@class='md-snackbar-content']//span[contains(text(), 'Your changes have been saved.')]"));

        private static readonly String ChannelCodecToggle = "div[data-tour={0}]";
        private static readonly String ChannelInputString = "input[data-tour=channel_{0}]";

        private static readonly By ChannelCardSfu = By.CssSelector("div[data-tour=channel_card_sfu]");
        private static readonly By ChannelCardAudio = By.CssSelector("div[data-tour=channel_card_audio]");
        private static readonly By ChannelCardVideo = By.CssSelector("div[data-tour=channel_card_video]");
        private static readonly By ChannelCardMcu = By.CssSelector("div[data-tour=channel_card_mcu]");


        IWebElement NewChannel => driver.FindElement(By.XPath("//div[@data-tour='application_card_channels']//table//tr[1]//td[1]//div"));

        public EditChannelPage(IWebDriver driver): base(driver)
        {
            this.driver = driver;
        }


        public void SetSfuConfig()
        {
            JsExecuteScroll(false, driver.FindElement(ChannelCardSfu));

            Clear(ChannelInput("sfu_audio_bitrate"));
            Type(ChannelInput("sfu_audio_bitrate"), Rng(1, 200));
            //Add random range between 1 to 200

            Clear(ChannelInput("sfu_video_bitrate"));
            Type(ChannelInput("sfu_video_bitrate"), Rng(1, 20000));
            //Add random range between 1 to 20,000

            ClickToSubmitChanges();
        }

/*        public void ClearSfuConfig()
        {
            JsExecuteScroll(true, driver.FindElement(ChannelCardSfu));

            Clear(ChannelInput("sfu_audio_bitrate"));
            Clear(ChannelInput("sfu_video_bitrate"));

            ClickToSubmitChanges();
        }*/

        public void UpdateAudioCodec()
        {
            JsExecuteScroll(true, driver.FindElement(ChannelCardAudio));

            //Click(AudioCodec("channel_opus"));
            Click(Codec("channel_pcma"));
            Click(Codec("channel_pcmu"));
            Click(Codec("channel_g722"));

            Clear(ChannelInput("opus_bitrate"));
            Type(ChannelInput("opus_bitrate"), "292");

            ClickToSubmitChanges();
        }

/*        public void ClearAudioCodecs()
        {
            JsExecuteScroll(true, driver.FindElement(ChannelCardSfu));

            Clear(ChannelInput("opus_bitrate"));

            ClickToSubmitChanges();
        }*/

        public void UpdateVideoCodec()
        {
            JsExecuteScroll(true, driver.FindElement(ChannelCardVideo));

            Click(Codec("vp9"));

            Clear(ChannelInput("vp8_bitrate"));
            Type(ChannelInput("vp8_bitrate"), Rng(1, 999));

            Clear(ChannelInput("h264_bitrate"));
            Type(ChannelInput("h264_bitrate"), Rng(1, 999));

            ClickToSubmitChanges();
        }

/*        public void ClearVideoCodecs()
        {
            JsExecuteScroll(true, driver.FindElement(ChannelCardSfu));

            Clear(ChannelInput("vp8_bitrate"));
            Clear(ChannelInput("h264_bitrate"));

            ClickToSubmitChanges();
        }*/

        public void SetMcuConfig()
        {
            JsExecuteScroll(false, driver.FindElement(ChannelCardMcu));

            Clear(ChannelInput("mcu_audio_bitrate"));
            Type(ChannelInput("mcu_audio_bitrate"), Rng(2, 199));
            //Add random range between 1 to 200

            Clear(ChannelInput("mcu_video_bitrate"));
            Type(ChannelInput("mcu_video_bitrate"), Rng(2, 19999));
            //Add random range between 1 to 20,000

            Clear(ChannelInput("layout_margin"));
            Type(ChannelInput("layout_margin"), Rng(2, 999));
            //Add random range between 1 to 999

            Clear(ChannelInput("frame_width"));
            Type(ChannelInput("frame_width"), Rng(161, 1919));
            //Add random range between 160 to 1920

            Clear(ChannelInput("frame_height"));
            Type(ChannelInput("frame_height"), Rng(121, 1079));
            //Add random range between 120 to 1080

            Clear(ChannelInput("frame_rate"));
            Type(ChannelInput("frame_rate"), Rng(2, 29));
            //Add random range between 1 to 30

            //TODO: Add once data-tour attribute has been added
            //JsExecuteClick(ChannelLayoutCrop);

            ClickToSubmitChanges();
        }

/*        public void ClearMcuConfig()
        {
            JsExecuteScroll(true, driver.FindElement(ChannelCardMcu));

            Clear(ChannelInput("mcu_audio_bitrate"));
            Clear(ChannelInput("mcu_video_bitrate"));
            Clear(ChannelInput("layout_margin"));
            Clear(ChannelInput("frame_width"));
            Clear(ChannelInput("frame_height"));
            Clear(ChannelInput("frame_rate"));

            ClickToSubmitChanges();
        }*/

        public bool ClearConfigSuccess()
        {
            return wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(ChannelInput("mcu_audio_bitrate"))).GetAttribute("value").IsNullOrEmpty(); 
        }

        private By Codec(String type)
        {
            return By.CssSelector(Template(ChannelCodecToggle, type));
        }

        private By ChannelInput(String type)
        {
            return By.CssSelector(Template(ChannelInputString, type));
        }

        public void ClickOnSubmit()
        
        {
            BtnSubmit.Click();
        }

        public bool UpdateSuccessMsgDisplayed()
        {
            return UpdateSuccessMsg.Displayed;
        }

        public void ClickOnChannel()
        {
            Thread.Sleep(2000);
            NewChannel.Click();
        }

    }
}
