﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;



namespace LiveSwitchCloudConsole
{
   class APIKeys_ApplicationPage : TestBase
    {
        AppcuesComponent obj;
        WebDriverWait wait;
        BasePage basePage;

        IWebElement BtnNew => driver.FindElement(By.XPath("//div[@class='md-button-content']//i[text()='add_circle']"));

        IWebElement TxtTag => driver.FindElement(By.XPath("//input[@class='swal2-input']"));

        IWebElement  UpdateTag => driver.FindElement(By.XPath("//input[@data-tour='application_tag']"));

        IWebElement BtnOK => driver.FindElement(By.XPath("//button[@class='swal2-confirm md-button md-success btn-fill']"));

        IWebElement BtnSubmit => driver.FindElement(By.XPath("//button[@data-tour='submit']"));


        string ApiKeyData = "//table//tr//td[1]//div[normalize-space()='#']";

        string BtnEdit = "//table//tr//td[1]//div[normalize-space()='#']//parent::td//following-sibling::td[2]/div/button[1]";
        string BtnDeleteAPIKeys = "//table//tr//td[1]//div[normalize-space()='#']//parent::td//following-sibling::td[2]/div/button[2]";
        string BtnDeleteApplication = "//table//tr//td[1]//div[normalize-space()='#']//parent::td//following-sibling::td[2]/div/button[2]";
        string ApplicationEdit = "//table//tr//td[1]//div[normalize-space()='#']//parent::td";

        IWebElement BtnDeleteConfirm => driver.FindElement(By.XPath("//button[text()='Yes, delete it!']"));

        IWebElement BtnDeleteOK => driver.FindElement(By.XPath("//button[text()='OK']"));

        IWebElement UpdateSuccessMsg => driver.FindElement(By.XPath("//div[@class='md-snackbar-content']//span[contains(text(), 'Your changes have been saved.')]"));




        public APIKeys_ApplicationPage(IWebDriver driver)
        {
            this.driver = driver;
            wait = new WebDriverWait(driver, new TimeSpan(0, 0, 60));
        }

        public void ClickOnNew()
        {
            BtnNew.Click();
        }

        public void EnterTagName(string tagName)
        {
            
            obj = new AppcuesComponent(driver);
            obj.CheckAppcuesNps();
            TxtTag.Clear();
            TxtTag.SendKeys(tagName);
        }


        public void EnterNewTagName(string tagName)
        {

            // obj = new AppcuesComponent(driver);
            //   obj.CheckAppcuesNps();
            basePage = new BasePage(driver);
            basePage.JsExecuteScroll(true, UpdateTag);
            UpdateTag.Clear();
            UpdateTag.SendKeys(tagName);
        }

        public void ClickOnOk()
        {
            BtnOK.Click();
        }

        public bool ValidateAPIKeyData(string keyName)
        {
           
           string actualPath = ApiKeyData.Replace("#", keyName);
           bool status = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.XPath(actualPath))).Displayed;

            return status;
        }

        public bool ValidateDeleteAPIKeyData(string keyName)
        {
            bool status = false;

            try
            {
                string actualPath = ApiKeyData.Replace("#", keyName);
                 status =  driver.FindElement(By.XPath(actualPath)).Displayed;
                 status = false;
            }
            catch (Exception)
            {
                status = true;
            }

            return status;
        }

        public void ClickOnEditButton(string keyName)
        {
            Thread.Sleep(1000);
            obj = new AppcuesComponent(driver);
            obj.CheckAppcuesNps();
            string actualPath = BtnEdit.Replace("#", keyName);
            driver.FindElement(By.XPath(actualPath)).Click();

        }

        public void ClickOnDeleteButtonAPIKeys(string keyName)
        {
            Thread.Sleep(1000);
            obj = new AppcuesComponent(driver);
            obj.CheckAppcuesNps();
            string actualPath = BtnDeleteAPIKeys.Replace("#", keyName);
            driver.FindElement(By.XPath(actualPath)).Click();

        }
        public void ClickOnDeleteButtonApplication(string keyName)
        {
            BasePage objBasePage = new BasePage(driver);
             obj = new AppcuesComponent(driver);
             obj.CheckAppcuesNps();
                string actualPath = BtnDeleteApplication.Replace("#", keyName);

                try
                {
                    driver.FindElement(By.XPath(actualPath)).Click();
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("element not interactable"))
                    {
                        IWebElement Element = driver.FindElement(By.XPath(actualPath));

                        objBasePage.JsExecuteScroll(false, Element);
                        Element.Click();
                    }

                }

        }

        public void ClickOnDeleteConfirm()
        {
            Thread.Sleep(2000);

            if (BtnDeleteConfirm.Displayed)
            {
                BtnDeleteConfirm.Click();
            }

        }
        
        public bool ClickOnDeleteOKButton()
        {
            bool status = false;
            if (BtnDeleteOK.Displayed)
            {
                status = true;
                BtnDeleteOK.Click();
            }

            return status;
        }


        public void ClickOnSubmit()
        {
            Thread.Sleep(1000);
            BtnSubmit.Click();
        }

        public void ClickOnApplicationEdit(string keyName)
        {
            BasePage objBasePage = new BasePage(driver);
            obj = new AppcuesComponent(driver);
            //obj.CheckAppcuesNps();
            string actualPath = ApplicationEdit.Replace("#", keyName);
            try
            {
                Thread.Sleep(2000);
                driver.FindElement(By.XPath(actualPath)).Click();
            }
            catch (Exception ex)
            {
               // if (ex.Message.Contains("element not interactable"))
              //  {
                    IWebElement Element = driver.FindElement(By.XPath(actualPath));

                    objBasePage.JsExecuteScroll(false, Element);
                    Element.Click();
                //}
               
            }

        }

        public bool SuccessMessage()
        {
            bool status = false;
            if (UpdateSuccessMsg.Displayed)
            {
                status = true;
            }

            return status;
        }



    }
}
