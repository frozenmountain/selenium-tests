﻿using OpenQA.Selenium;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace LiveSwitchCloudConsole
{
    public class SubscriptionPage : BasePage
    {
        private static readonly By SubscriptionLi = By.CssSelector("li[data-tour='nav_subscription']");
        private static readonly string BadgeSpan = "//span[contains(text(), '{0}')]";

        private static readonly By CancelSubscriptionLink = By.XPath("//a[contains(text(), 'Cancel My Subscription')]");
        private static readonly By ConfirmCancellationBtn = By.XPath("//button[contains(text(), 'Yes, cancel it!')]");
        private static readonly By CloseCancelledModalBtn = By.XPath("//button[contains(text(), 'OK')]");

        private static readonly By EditSupportImg = By.CssSelector("img[class='hs-cta-img']");

        private static readonly By SupportFreeBtn = By.CssSelector("input[value='support-plan-free-v1']");
        private static readonly By SupportGrowthBtn = By.CssSelector("input[value='sup-monthly-gr-req-v1']");
        private static readonly By SupportCardSubmitBtn = By.CssSelector("button[data-cb-id='cart_submit']");
        private static readonly By SupportConfirmBtn = By.CssSelector("button[class='swal2-confirm md-button md-success btn-fill']");

        private static readonly By CbReviewSubmitBtn = By.CssSelector("button[data-cb-id='review_submit']");
        private static readonly By CloseModalBtn = By.CssSelector("button[data-cb-id='cb_close']");

        public SubscriptionPage(IWebDriver driver) : base(driver)
        {
            try
            {
                GoToPage();
            }
            catch(WebDriverException)
            {
                ;
            }
        }

        public void Cancel()
        {
            Click(CancelSubscriptionLink);
            Click(ConfirmCancellationBtn);
            Click(CloseCancelledModalBtn);
        }

        public void RemoveSupport()
        {
            Click(EditSupportImg);

            Thread.Sleep(5000);
            JsExecuteClick(SupportFreeBtn);
            JsExecuteClick(SupportCardSubmitBtn);
            //Same class buttons to close modals
            JsExecuteClick(SupportConfirmBtn);
            JsExecuteClick(SupportConfirmBtn);
        }

        public void ChangeSupport()
        {
            JsExecuteClick(EditSupportImg);
            
            Thread.Sleep(5000);
            JsExecuteClick(SupportGrowthBtn);
            JsExecuteClick(SupportCardSubmitBtn);

            SwitchToChargebeeFrame();
            JsExecuteClick(CbReviewSubmitBtn);
            JsExecuteClick(CloseModalBtn);

            SwitchToDefault();
        }

        public void GoToPage()
        {
            Click(SubscriptionLi);
        }

        public bool CancelledSubscriptionSuccess()
        {
            //store in db
            return wait.Until(ExpectedConditions.ElementIsVisible(StatusToBe(Db().GetField("CancelledStatus")))).Displayed;
        }

        public bool ActiveStatus()
        {
            //store in db
            return wait.Until(ExpectedConditions.ElementIsVisible(StatusToBe(Db().GetField("Status")))).Displayed;

        }

        public bool SupportStatus()
        {
            return wait.Until(ExpectedConditions.ElementIsVisible(StatusToBe(Db().GetField("SupportStatus")))).Displayed;
        }

        private By StatusToBe(string status)
        {
            return By.XPath(String.Format(BadgeSpan, status));
        }
    }
}
