﻿using OpenQA.Selenium;
using System.Threading;

namespace LiveSwitchCloudConsole
{
    class HomeScreen : BasePage
    {
        IWebDriver driver;
        IWebElement TxtMyAccount => driver.FindElement(By.XPath("//span[text()='Account']"));
        IWebElement TxtMyDownload => driver.FindElement(By.XPath("//h1[text()='Downloads']"));
        
        IWebElement CookiesOkBtn => driver.FindElement(By.XPath("//div[@id='hs-en-cookie-confirmation-buttons-area']"));
        IWebElement TxtUserName => driver.FindElement(By.XPath("//h2[@class='blue']/strong"));
        IWebElement TxtLetGetStarted => driver.FindElement(By.XPath("//a[@class='checkout-cta button theme']"));
        IWebElement BtnAPIKeys => driver.FindElement(By.XPath("//a[@href='/apikeys']"));
        IWebElement BtnApplication => driver.FindElement(By.XPath("//a[@href='/applications']"));     
        IWebElement DropDownFm => driver.FindElement(By.XPath("//button[@data-tour='top_dropdown']"));    
        IWebElement Logout_Environment => driver.FindElement(By.XPath("//span[normalize-space()='Logout']"));
        IWebElement Logout_FM => driver.FindElement(By.XPath("//a[text()='Logout']"));
        IWebElement Billing => driver.FindElement(By.XPath("//span[normalize-space()='Billing']"));

        IWebElement BtnDownloads => driver.FindElement(By.XPath("//a[@href='/downloads']"));

        private static readonly By DashboardLi = By.CssSelector("li[data-tour='nav_dashboard']");
        private static readonly By ProfileUl = By.CssSelector("ul[data-tour='nav_user']");
        private static readonly By frame = By.XPath("//iframe[@id='hsIframe']");
       

        public HomeScreen(IWebDriver driver) : base(driver)
        {
            this.driver = driver;
        }

        public void SwitchToFrame()
        {
            SwitchToFrame(frame);
        }

        public void ClearCookies()
        {
            Thread.Sleep(4000);         
          CookiesOkBtn.Click();
           
        }

        public bool GetMyAccounttxt()
        {
            bool txt = TxtMyAccount.Displayed;

            return txt;

        }

        public bool GetMyAccounttxtDownload()
        {
            bool txt = TxtMyDownload.Displayed;

            return txt;

        }
        public bool GetMyAccounttxtPurchase()
        {
            bool txt = TxtMyAccount.Displayed;

            return txt;

        }
        public string GetUserName()
        {
            string txt = TxtUserName.Text;

            return txt;

        }

        public bool GetLetGetStarted()
        {
            bool txt = TxtLetGetStarted.Displayed;

            return txt;

        }
        public void ClickOnAPIKeys()
        {
            BtnAPIKeys.Click();
        
        }

        public void ClickOnApplication()
        {
            BtnApplication.Click();
        }

        public void GoToDashboard()
        {
            Click(DashboardLi);
        }

        public ProfilePage GoToProfile()
        {
            Click(ProfileUl);
            return new ProfilePage(driver);
        }

        public void FrozenMountainDropDown()
        {
            Thread.Sleep(1000);
            DropDownFm.Click();
        }

        public void LogoutButton_Environment()
        {
            Thread.Sleep(1000);
            Logout_Environment.Click();
        }

        public void LogoutButton_FM()
        {
            Thread.Sleep(1000);
            Logout_FM.Click();
        }

        public void ClickOnBilling()
        {
            Thread.Sleep(1000);
            Billing.Click();
        }

        public void ClickOnDownloads()
        {
            BtnDownloads.Click();
        }

    }
}
