﻿using OpenQA.Selenium;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace LiveSwitchCloudConsole
{
    public class SubscriptionModal : BasePage
    {
        protected IWebDriver driver;
        private static readonly String CtaBtn = "//a[contains(text(), \"{0}\")]";
        private static readonly By ReviewSubmitBtn = By.CssSelector("button[data-cb-id='review_submit']");
        private static readonly By CloseModalBtn = By.CssSelector("button[data-cb-id='cb_close']");
        private static readonly By AppcuesIntroCloseBtn = By.CssSelector("a[data-step='end']");

        private static readonly By CbReactivationBtn = By.CssSelector("button[data-cb-id='review_submit']");
        private static readonly By CbCreditCardInput = By.CssSelector("input[name='cc-number']");
        private static readonly By CbCredCardValidDiv = By.CssSelector("div[class='cb-pmcard__option']");

        private static readonly By CbEstimateBar = By.CssSelector("div[data-cb-id='estimate-fixed']");

        public SubscriptionModal(IWebDriver driver) : base(driver)
        {
            this.driver = driver;
            SwtichToHubspotFrame();
        }

        public void ActivateTrial()
        {
            Click(Cta("InitializeBtn"));

            SwitchToChargebeeFrame();

            JsExecuteClick(ReviewSubmitBtn);
            JsExecuteClick(CloseModalBtn);
        }

        public void ActivateSubscription()
        {
            JsExecuteClick(Cta("InitializeBtn"));

            SwitchToChargebeeFrame();

            JsExecuteClick(CbCreditCardInput);
            wait.Until(ExpectedConditions.ElementToBeClickable(CbCreditCardInput)).SendKeys(Keys.Tab);
            driver.FindElements(CbCredCardValidDiv)[0].SendKeys(Keys.Return);
            JsExecuteClick(CbReactivationBtn);
            JsExecuteClick(CloseModalBtn);
        }

        public void ReactivateSubscription()
        {
            SwtichToHubspotFrame();

            JsExecuteClick(Cta("ActivationBtn"));

            SwitchToChargebeeFrame();

            JsExecuteClick(ReviewSubmitBtn);
            JsExecuteClick(CloseModalBtn);

            SwitchToDefault();
        }

        public void CloseModal()
        {
            SwitchToTopFrame();
            Thread.Sleep(5000);
            JsExecuteClick(AppcuesIntroCloseBtn);
        }

        public void CheckoutPayment()
        {
            SwitchToChargebeeFrame();

            JsExecuteClick(ReviewSubmitBtn);
            JsExecuteClick(CloseModalBtn);
        }

        public bool ActivationSuccess()
        {
            SwitchToTopFrame();
            return wait.Until(ExpectedConditions.ElementIsVisible(AppcuesIntroCloseBtn)).Displayed;
        }

        public bool ReactivationSuccess()
        {
            SwitchToChargebeeFrame();
            return wait.Until(ExpectedConditions.ElementIsVisible(CbEstimateBar)).Displayed;
        }

        private By Cta(String val)
        {
            return By.XPath(String.Format(CtaBtn, Db().GetField(val)));
        }
    }
}
