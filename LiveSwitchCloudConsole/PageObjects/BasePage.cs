using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace LiveSwitchCloudConsole
{
    public class BasePage
    {
        private readonly IWebDriver driver;
        protected WebDriverWait wait;
        private static readonly By frame = By.XPath("//iframe[@id='hsIframe']");

        private static readonly By cbframe = By.XPath("//iframe[@id='cb-frame']");

        private static readonly By CookieBtn = By.Id("hs-eu-confirmation-button");

        private static readonly By SubmitUpdateBtn = By.Id("snackbar-submit");
        public BasePage(IWebDriver driver)
        {
            this.driver = driver;
            wait = new WebDriverWait(driver, new TimeSpan(0, 0, 60));
        }
        protected void Type(By locator, String val)
        {
            IWebElement input = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(locator));
            input.SendKeys(val);
        }

        protected void Clear(By locator)
        {
            IWebElement input = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(locator));
            input.Clear();

        }

        protected void Click(By locator)
        {
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(locator));
            element.Click();
        }

        protected void SwitchToFrame(By locator)
        {
          wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.FrameToBeAvailableAndSwitchToIt(locator));
           
        }

        protected void Cookie()
        {
            try
            {
                IWebElement cookie = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(CookieBtn));
                cookie.Click();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("No cookie present" + e);
            }
        }

        protected void JsExecuteClick(By locator)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(locator));
            
            js.ExecuteScript("arguments[0].click();", element);
        }

        protected void JsExecuteClick(IWebElement element)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;

            js.ExecuteScript("arguments[0].click();", element);
        }

      

       public void JsExecuteScroll(bool boolean, IWebElement webElement)
        {

            string Value = boolean.ToString().ToLower();
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("arguments[0].scrollIntoView(" + Value + ");", webElement);

        }

        public string Template(string template, string val)
        {
            return String.Format(template, val);
        }

        protected void ClickToSubmitChanges()
        {   //driver switch is too fast to keep up with the modal load
            Thread.Sleep(3000);
            Click(SubmitUpdateBtn);
        }

        protected void SwtichToHubspotFrame()
        {
            //driver switch is too fast to keep up with the modal load
            Thread.Sleep(3000);
            SwitchToFrame(frame);
        }

        protected void SwitchToChargebeeFrame()
        {
            Thread.Sleep(3000);
            driver.SwitchTo().DefaultContent();
            SwitchToFrame(cbframe);
        }

        protected void SwitchToTopFrame()
        {
            //Inncreasing sleep time to handle stale elements
            Thread.Sleep(9000);
            //IList<IWebElement> obj = driver.FindElements(By.TagName("iframe"));
            //driver.SwitchTo().DefaultContent();
            
            driver.SwitchTo().Frame(0);
        }

        protected void SwitchToParentFrame()
        {
            //Inncreasing sleep time to handle stale elements
            Thread.Sleep(7000);
            //driver.SwitchTo().DefaultContent();
            driver.SwitchTo().ParentFrame();
        }

        protected void SwitchToDefault()
        {
            driver.SwitchTo().DefaultContent();
        }

        protected string Date()
        {
            return DateTime.Now.ToString("MM-dd-yy");
        }

        protected String Rng(int startRange, int endRange)
        {
            return new Random().Next(startRange, endRange).ToString();
        }

        public MongoDbManager Db()
        {
            return new MongoDbManager();
        }

        protected WebhookComponent Webhook()
        {
            return new WebhookComponent(driver);
        }

        public String Rand()
        {
            return new Random().Next(100, 9999).ToString();
        }

    }
}
