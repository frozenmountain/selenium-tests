﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;
using Xunit.Sdk;

namespace LiveSwitchCloudConsole
{
    public class LoginPage : BasePage
    {
        protected IWebDriver driver;
        private static readonly By emailInput = By.Name("email");
        private static readonly By pwdInput = By.Name("password");
        private static readonly By loginBtn = By.CssSelector("button[type='submit']");

        private static readonly By dropdownBtn = By.CssSelector("button[data-tour='top_dropdown']");

        private static readonly By myAccountSection = By.ClassName("my-account");
 
        public LoginPage(IWebDriver driver) : base(driver)
        {
            this.driver = driver;
            Cookie();
        }

        public void UserCredentials()
        {
            Type(emailInput, Db().GetField("Username"));
            Type(pwdInput, Db().GetField("Password"));
            Click(loginBtn);
        }

        public void NewUserCredentials(String val)
        {
            Type(emailInput, val);
            Type(pwdInput, "Test-12345");
            Click(loginBtn);
        }

        public bool Success()
        {
            return wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(dropdownBtn)).Displayed;
        }

        public bool IsLoginButtonDisplayed()
        {
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(loginBtn));

            return element.Displayed;

        }
    
    }
}
