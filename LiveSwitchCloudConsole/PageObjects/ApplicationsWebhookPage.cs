﻿using LiveSwitchCloudConsole.Utility;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace LiveSwitchCloudConsole
{
    public class ApplicationsWebhookPage :  BasePage
    {
        protected IWebDriver driver;
        private static readonly By ApplicationsLi = By.CssSelector("li[data-tour='nav_applications']");

        private static readonly By AppTagDiv = By.CssSelector("div[data-tour='application_card_tag']");

        private static readonly By CreateAppBtn = By.CssSelector("button[data-tour='application_new']");

        private static readonly By EditAppBtn = By.CssSelector("[data-tour='application_edit']");

        private static readonly By WebhookApplicationDiv = By.CssSelector("div[data-tour='application_card_webhooks']");
        private static readonly By WebhookApplicationProd = By.XPath("//button[@data-tour='webhook_new']");
        

        private static readonly By ChannelApplicationDiv = By.CssSelector("div[data-tour='application_card_channels']");

        private static readonly By ChannelEditBtn = By.CssSelector("[data-tour='channel_edit']");

        private static readonly By SubmitBtn = By.XPath("//button[@data-tour='submit']");

       
        public ApplicationsWebhookPage(IWebDriver driver) : base(driver)
        {
            this.driver = driver;
            //Click(ApplicationsLi);
            //wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(CreateAppBtn));
            //EditMyApp();
        }

        public void CreateNewWebhook()
        {
            IWebElement WebhookApplication = null;
            String env = new ConfigurationManager().Env();
            if (env.Equals("Phoenix") || env.Equals("Ashburn"))
            {
                
                WebhookApplication = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(WebhookApplicationProd));
            }
            else
            {
                WebhookApplication = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(WebhookApplicationDiv));
            }
            //JsExecuteScroll(true, WebhookApplication);
            Webhook().WebhookCreate();
        }

        public void UpdateWebhook()
        {
            Webhook().Update();
            Click(SubmitBtn);
        }

        public void DeleteWebhook()
        {
            Webhook().Delete();
        }

        public void FilterDb()
        {
            Db().GetTestData("ApplicationsWebhookTest");
        }

        public void FilterDb(String test)
        {
            Db().GetTestData(test);
        }

        private void EditMyApp()
        {
            JsExecuteClick(EditAppBtn);
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(AppTagDiv));
        }

        public ChannelPage EditChannel()
        {
            BasePage objBasePage = new BasePage(driver);
            try
            {
                Click(ChannelEditBtn);
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(ChannelApplicationDiv));
                return new ChannelPage(driver);
            }
            catch (Exception ex)
            {
                
                objBasePage.JsExecuteScroll(false, driver.FindElement(ChannelEditBtn));

                Click(ChannelEditBtn);
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(ChannelApplicationDiv));
                return new ChannelPage(driver);

            }

        }

    }
}
