﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Threading;

namespace LiveSwitchCloudConsole
{
    class SignUpPage : TestBase
    {
        
        WebDriverWait Wait;
        IWebElement CookiesOkBtn => driver.FindElement(By.CssSelector("div[id='hs-en-cookie-confirmation-buttons-area']"));

        IWebElement TxtUsername => driver.FindElement(By.CssSelector("input[name='email']"));
        IWebElement TxtPassword => driver.FindElement(By.CssSelector("input[name='password']"));
        IWebElement TxtFirstName => driver.FindElement(By.CssSelector("input[name='first_name']"));
        IWebElement TxtLastName => driver.FindElement(By.CssSelector("input[name='last_name']"));
        IWebElement TxtCompany => driver.FindElement(By.CssSelector("input[name='company']"));
        IWebElement TxtPhoneNumber => driver.FindElement(By.CssSelector("input[name='phone_number']"));
        IWebElement BtnCountry => driver.FindElement(By.CssSelector("input[name='country']"));
        IWebElement TxtSearchDropDown => driver.FindElement(By.CssSelector("input[name='search']"));

        IWebElement BtnSubmit => driver.FindElement(By.CssSelector("button[type='submit']"));

        

        IWebElement BtnIndustry => driver.FindElement(By.CssSelector("input[name='industry']"));
        IWebElement BtnRole => driver.FindElement(By.CssSelector("input[name='role']"));

        IWebElement BtnChkbox => driver.FindElement(By.XPath("//div[@class='auth0-lock-input-checkbox']//input[@type='checkbox']"));
        

        private static readonly By InputLocator = By.CssSelector("input[name='email']");
        private static readonly By DropdownBtn = By.CssSelector("button[data-tour='top_dropdown']");

        public SignUpPage(IWebDriver driver)
        {
            this.driver = driver;
            Wait = new WebDriverWait(driver, new TimeSpan(0, 0, 60));
        }

        public void EnterUserName(string userName)
        {
            Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(InputLocator));
            TxtUsername.SendKeys(userName);
        }

        public void EnterPassword(string pwd)
        {
            TxtPassword.SendKeys(pwd);
        }

        public void EnterFirstName(string firstName)
        {
            TxtFirstName.SendKeys(firstName);


        }
        public void EnterLastName(string lastName)
        {

            TxtLastName.SendKeys(lastName);
        
        }


        public void EnterCompany(string company)
        {
            TxtCompany.SendKeys(company);
        }

        public void EnterPhoneNumber(string phoneNumber)
        {
            TxtPhoneNumber.SendKeys(phoneNumber);
        }

        public void SelectCountry(String Country)
        {
            BtnCountry.Click();
            TxtSearchDropDown.SendKeys(Country);
            SelectDropdown(Country);
        }

        public void SelectIndustry(string industry)
        {
            BtnIndustry.Click();
            TxtSearchDropDown.SendKeys(testDataService["Industry"].ToString());
            SelectDropdown(testDataService["Industry"].ToString());

        }
        public void SelectRole(string role)
        {
            BtnRole.Click();
            TxtSearchDropDown.SendKeys(testDataService["Role"].ToString());
            SelectDropdown(testDataService["Role"].ToString());

        }

        public void CheckPrivacyPolicy(string consent)
        {
            CheckboxPrivacyPolicy(consent);

        }
        public void ClickSubmitButton()
        {
            BtnSubmit.Click();
        }
      
        public void SelectDropdown(string value)
        {

            string searchXpath = "//li[text() ='$VALUE']";

            string actualPath = searchXpath.Replace("$VALUE", value);

            driver.FindElement(By.XPath(actualPath)).Click();

        }

        public void CheckboxPrivacyPolicy(string consent)
        {

            String txt = consent;
            if(txt.ToLower().Equals("false"))
            {
                BtnChkbox.Click();

            }

        }

        public void ClickOnCookies()
        {
            CookiesOkBtn.Click();
        }

        public LoginPage NavigateToLogin()
        {
            Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(DropdownBtn));
            Thread.Sleep(2000);
            driver.Navigate().GoToUrl(Db().GetField("LoginURL"));
            return new LoginPage(driver);
        }
    }
}
