﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace LiveSwitchCloudConsole
{
    

    class DownloadsPage:BasePage
    {
        IWebDriver driver;

        public DownloadsPage(IWebDriver driver) : base(driver)
        {
            this.driver = driver;
        }


        public void ClickOnAlphaDownloads()
        {

            IList<IWebElement> BtnDownloads = driver.FindElements(By.XPath("//h4[text()='Client SDK Downloads']/parent::div/parent::div/parent::div//p/a"));
            BtnDownloads[1].Click();

        }

        public void ClickOnBetaDownloads()
        {

            IList<IWebElement> BtnDownloads = driver.FindElements(By.XPath("//h4[text()='Beta Client SDK Downloads']/parent::div/parent::div/parent::div//p/a"));
            BtnDownloads[1].Click();

        }

    }
}
