﻿using OpenQA.Selenium;

namespace LiveSwitchCloudConsole
{
    class LandingPage : TestBase
    {
        
        IWebElement BtnFreeTrial => driver.FindElement(By.XPath("//a[text()='Free Trial']"));

        IWebElement BtnSign_up => driver.FindElement(By.XPath("//a[text()='Sign Up']"));

        IWebElement BtnSignup => driver.FindElement(By.XPath("//a[text()='Signup']"));


        public LandingPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public void ClickOnFreeTrial()
        {

            BtnFreeTrial.Click();

        }


        public void Sign_up_FM()
        {

            BtnSign_up.Click();

        }

        public void Signup_Environment()
        {

            BtnSignup.Click();

        }

        public bool IsFreeTrailButtonDisplayed()
        {
            return BtnFreeTrial.Displayed;
        }
        
    }
}
