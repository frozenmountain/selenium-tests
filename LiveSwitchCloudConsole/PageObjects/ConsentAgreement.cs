﻿using OpenQA.Selenium;
using System;
using System.Threading;

namespace LiveSwitchCloudConsole
{ 
    class ConsentAgreement : TestBase 
    {
        IWebElement BtnAgreement => driver.FindElement(By.XPath("//input[@type='checkbox']"));
        IWebElement BtnSubmit1 => driver.FindElement(By.CssSelector("button[type='submit']"));
        
        public ConsentAgreement(IWebDriver driver)
        {
            this.driver = driver;
            
        }

        public void Agreement()
        {
            String txt = testDataService["Consent"].ToString();
            if (txt.ToLower().Equals("false"))
            {
                Thread.Sleep(8000);
                driver.Navigate().Refresh();
                BtnAgreement.Click();
                BtnSubmit1.Click();

            }
        }
    }
}