﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace LiveSwitchCloudConsole
{
    public class ProfilePage : BasePage
    {
        protected IWebDriver driver;
        private static readonly By firstNameInput = By.CssSelector("input[data-tour='first_name']");
        private static readonly By lastNameInput = By.CssSelector("input[data-tour='last_name']");
        private static readonly By phoneNumberInput = By.CssSelector("input[data-tour='phone_number']");

        private static readonly By submitButton = By.CssSelector("button[data-tour='submit']");
        private static readonly By updateSuccessMsg = By.XPath("//div[@class='md-snackbar-content']//span[contains(text(), 'Your changes have been saved.')]");

        public ProfilePage(IWebDriver driver) : base(driver)
        {
            this.driver = driver;
        }

        public void Update()
        {

            Clear(firstNameInput);
            Type(firstNameInput, Db().GetField("Firstname") + " Update " + Date());

            Clear(lastNameInput);
            Type(lastNameInput, Db().GetField("Lastname") + " Update " + Date());

            Clear(phoneNumberInput);
            Type(phoneNumberInput, "604" + Rng(1000000, 9999999));

            ClickToSubmitChanges();
        }

        public Boolean UpdateSuccess()
        {
            return wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(updateSuccessMsg)).Displayed;
        }

    }
}
