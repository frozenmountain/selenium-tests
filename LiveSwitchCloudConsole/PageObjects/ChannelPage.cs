﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Threading;

namespace LiveSwitchCloudConsole
{
    public class ChannelPage : BasePage
    {
        IWebDriver driver;

        IWebElement BtnSubmit => driver.FindElement(By.XPath("//button[@data-tour='submit']"));
        IWebElement  TxtApplicationTag => driver.FindElement(By.XPath("//input[@data-tour='application_tag']"));


        IWebElement BtnAddChannel => driver.FindElement(By.XPath("//button[@data-tour='channel_new']"));
        IWebElement TxtTag => driver.FindElement(By.XPath("//input[@class='swal2-input']"));

        IWebElement BtnOK => driver.FindElement(By.XPath("//button[@class='swal2-confirm md-button md-success btn-fill']"));

        IWebElement BtnDeleteConfirm => driver.FindElement(By.XPath("//button[text()='Yes, delete it!']"));

        IWebElement BtnDeleteOK => driver.FindElement(By.XPath("//button[text()='OK']"));


        string ChannelID = "//div[@data-tour='application_card_channels']//table//tr//td[1]//div";

        private static readonly By WebhookChannelDiv = By.CssSelector("[data-tour='channel_card_webhook']");

        IWebElement EditChannelBtn => driver.FindElement(By.XPath("//div[@data-tour='application_card_channels']//tr[@class='md-table-row channel handle disabled']//td[2]//button[1]"));

        IWebElement ChannelEdit => driver.FindElement(By.XPath("//div[@data-tour='application_card_channels']//tr[@class='md-table-row channel handle md-has-selection disabled']"));

        


        public ChannelPage(IWebDriver driver) : base(driver)
        {
            this.driver = driver;
        }


        public void ClickOnSubmit()
        {
            BtnSubmit.Click();
        }


        public void EditApplicationTag(string keyName)
        {

            TxtApplicationTag.Clear();
            TxtApplicationTag.SendKeys(keyName);
            
        }

        public void ClickOnNew()
        {

            BtnAddChannel.Click();
        }

        public void EnterChannelID(string tagName)
        {
            TxtTag.Clear();
            TxtTag.SendKeys(tagName);
        }

        public void ClickOnOk()
        {

            BtnOK.Click();
        }


        public bool ValidateChannelData(string keyName)
        {
            Thread.Sleep(3000);
           
            bool status = false;
          IList<IWebElement> ele =  driver.FindElements(By.XPath(ChannelID));

            foreach (var i in ele)
            {
               string val =  i.Text;
               string value = val.Split("\n")[1];
                
                if (keyName.Equals(value))
                {             
                    status = true;
                    break;            
                }
            
            }
        
            return status;
        }


        public bool DeleteChannelData(string keyName)
        {
            
            bool status = false;
            Thread.Sleep(5000);

             IList<IWebElement> ele = driver.FindElements(By.XPath(ChannelID));

            foreach (var i in ele)
            {
                string val = i.Text;
                string value = val.Split("\n")[1];

                if (keyName.Equals(value))
                {
                    var delete =  i.FindElement(By.XPath("parent::td//following-sibling::td/div/button[1]"));
                    JsExecuteScroll(true, delete);
                    delete.Click();
                    Thread.Sleep(1000);

                    if (BtnDeleteConfirm.Displayed)
                    {
                        BtnDeleteConfirm.Click();
                    }
                    

                    if (BtnDeleteOK.Displayed)
                    {
                        status = true;
                        BtnDeleteOK.Click();
                    }
                             
                    break;
                }

            }

            return status;
    
        }


        public bool ValidateDeleteChannel(string keyName)
        {
            Thread.Sleep(1000);

            bool status = false;
            IList<IWebElement> ele = driver.FindElements(By.XPath(ChannelID));

            foreach (var i in ele)
            {
                string val = i.Text;
                string value = val.Split("\n")[1];

                if (keyName.Equals(value))
                {

                    status = true;
                    break;


                }

            }


            return status;
        }

        public void EditChannel()
        {
            EditChannelBtn.Click();
        }

        //CHANNEL WEBHOOK METHODS
        public void CreateWebhook()
        {
            //JsExecuteScroll(true, wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(WebhookChannelDiv)));
            Webhook().WebhookCreate();

        }

        public void UpdateWebhook()
        {
            Webhook().Update();
            ClickOnSubmit();
        }

        public void DeleteWebhook()
        {
            Webhook().Delete();
        }

        public void FilterDb()
        {
            Db().GetTestData("ChannelWebhookTest");
        }

        public void FilterDb(string test)
        {
            Db().GetTestData(test);
        }
    }
}
