﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;

namespace LiveSwitchCloudConsole.Utility
{
    public class TestFixture : TestBase, IDisposable
    {
        public static ExtentTest test;
        public static AventStack.ExtentReports.ExtentReports _extent;


        public TestFixture()
        {
            db = new MongoDbManager();
            
            string pth = System.Reflection.Assembly.GetCallingAssembly().CodeBase;

            string actualPath = pth.Substring(0, pth.LastIndexOf("bin"));

            string projectPath = new Uri(actualPath).LocalPath;

            string reportPath = projectPath + "Reports\\TestRunReport.html";
            string HostName = Dns.GetHostName();

            ExtentV3HtmlReporter htmlReporter = new ExtentV3HtmlReporter(reportPath);

            _extent = new AventStack.ExtentReports.ExtentReports();
            _extent.AttachReporter(htmlReporter);

            _extent.AddSystemInfo("HostName:", HostName);
            _extent.AddSystemInfo("Broswer:", new ConfigurationManager().Browser());
            _extent.AddSystemInfo("Environment:", new ConfigurationManager().Env());
        }

        public void Dispose()
        {
            Console.WriteLine("Teardown");
            _extent.Flush();


        }

    }
}
