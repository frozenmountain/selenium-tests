﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace LiveSwitchCloudConsole.Utility
{
    public class ConfigurationManager
    {
        public String Env()
        {
            return ParseConfig("environment");
        }

        public String Browser()
        {
            return ParseConfig("browser");
        }

        private String ParseConfig(String key)
        {
            JObject jObject = JObject.Parse(File.ReadAllText("../../../config.json"));
            JToken token = jObject[key];
            String val = token.ToString();
            return val;
        }
    }
}
