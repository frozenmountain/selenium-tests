﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Text;

namespace LiveSwitchCloudConsole.Utility
{
    public class BrowserFactory
    {
        public IWebDriver driver;

        private String BrowserType()
        {
            return new ConfigurationManager().Browser();
        }

        public IWebDriver CustomDriver()
        {

            String type = BrowserType();

            if(type == "Chrome")
            {
                driver = new ChromeDriver();
            }
            if(type == "Firefox")
            {
                driver = new FirefoxDriver();
            }
            /*
            if(type == "Safari")
            {
                driver = new SafariSafariOptions();
            }
             */

            return driver;

        }

        public IWebDriver CustomDriver(String incognito)
        {
            String type = BrowserType();

            if(type == "Chrome")
            {
                driver = ChromeOptions(incognito);
            }
            if(type == "Firefox")
            {
                driver = FirefoxOptions(incognito);
            }
            /*
            if(type == "Safari")
            {
                driver = SafariOptions(incognito);
            }
             */

            return driver;

        }

        private IWebDriver ChromeOptions(String incognito)
        {

            if (incognito.ToLower().Equals("true"))
            {
                ChromeOptions options = new ChromeOptions();
                options.AddArgument("--incognito");
                driver = new ChromeDriver(options);
            }
            else
            {
                driver = new ChromeDriver();
            }

            return driver;
            
        }

        private IWebDriver FirefoxOptions(String incognito)
        {

            if (incognito.ToLower().Equals("true"))
            {
                FirefoxOptions options = new FirefoxOptions();
                options.AddArgument("-private");
                driver = new FirefoxDriver(options);
            }
            else
            {
                driver = new FirefoxDriver();
            }

            return driver;

        }

        //add options for safari

    }
}
