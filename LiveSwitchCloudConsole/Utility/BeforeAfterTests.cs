﻿using AventStack.ExtentReports;
using LiveSwitchCloudConsole.Utility;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Xunit.Sdk;

namespace LiveSwitchCloudConsole
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]

    class BeforeAfterTests : BeforeAfterTestAttribute
    {
        public override void Before(MethodInfo methodUnderTest)
        {
            TestBase.testDataService.Clear();
            TestBase.testCaseService.Clear();

            TestFixture.test = TestFixture._extent.CreateTest(methodUnderTest.Name);
           

        }

        public override void After(MethodInfo methodUnderTest)
        {
            if (TestBase.status)
            {
                TestFixture.test.Log(Status.Pass, "Test Passed");
            }
            else
            {
                TestFixture.test.Log(Status.Fail, "Test Failed");
            }
            //Rollback the status to defaut value
            TestBase.status = false;
        }

    }
}

