﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;

namespace LiveSwitchCloudConsole
{
    public class MongoDbManager : TestBase
    {

        MongoClient dbClient;
        IMongoDatabase db;
        public MongoDbManager()
        {
            dbClient = new MongoClient("mongodb://192.168.124.117:27017");
            db = dbClient.GetDatabase("LiveSwitchAutomation");

        }


        public void GetTestData(string testCaseName)
        {
           var collection = db.GetCollection<BsonDocument>("Testdata");
           var filters = Builders<BsonDocument>.Filter.Eq("TestCaseName", testCaseName);
           var results =  collection.Find(filters).ToList();

            var data = results[0];
            testDataService =  data.ToDictionary();

        }


        public void GetTestCase(string testCaseName)
        {
            var collection = db.GetCollection<BsonDocument>("Testcases");
            var filters = Builders<BsonDocument>.Filter.Eq("TestCaseName", testCaseName);
            var results = collection.Find(filters).ToList();

            var data = results[0];
            testCaseService = data.ToDictionary();

        }

        public void Filter()
        {
            //Used for filtering general Test Cases with an environment readable by config file
            GetFilter("Environment", Config().Env(), "TestCaseName", "CanLogin");
        }

        public void Filter(String test)
        {
            //Used for filtering by TestCaseName with an environment readable by config file
            GetFilter("Environment", Config().Env(),"TestCaseName", test);
        }

        public void Filter(String env, String test)
        {
            //Used for  filtering a specific environment with a specific TestCaseName
            GetFilter("Environment", env, "TestCaseName", test);
        }
        

        public String GetField(String FieldName)
        {
            return testDataService[FieldName].ToString();
        }

        private void GetFilter(String field, String value)
        {
            var collection = db.GetCollection<BsonDocument>("Testdata");
            var filters = Builders<BsonDocument>.Filter.Eq(field, value);

            //Filter DB by one field
            var results = collection.Find(filters).ToList();
            var data = results[0];
            
            testDataService = data.ToDictionary();
        }

        
        private void GetFilter(String field1, String value1, String field2, String value2)
        {
            var collection = db.GetCollection<BsonDocument>("Testdata");
            var filter1 = Builders<BsonDocument>.Filter.Eq(field1, value1);
            var filter2 = Builders<BsonDocument>.Filter.Eq(field2, value2);

            //Filter DB by two fields
            var results = collection.Find(filter1 & filter2).ToList();
            var data = results[0];

            testDataService = data.ToDictionary();
        }

        public void GetGlobalData(string ID)
        {
            var collection = db.GetCollection<BsonDocument>("GlobalTestData");
            var filters = Builders<BsonDocument>.Filter.Eq("GlobalTestDataID", ID);
            var results = collection.Find(filters).ToList();

            var data = results[0];
            GlobalDataService = data.ToDictionary();

        }

    }
}
