﻿using LiveSwitchCloudConsole.Utility;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Threading;

namespace LiveSwitchCloudConsole
{
   public  class TestBase : IDisposable
    {

        protected IWebDriver driver;
        public static bool status;
        public static Dictionary<string, object> testDataService = new Dictionary<string, object>();
        public static Dictionary<string, object> testCaseService = new Dictionary<string, object>();
        public static Dictionary<string, object> GlobalDataService = new Dictionary<string, object>();
        public static MongoDbManager db;

        public void LauchBrowser(String incognito)
        {
            driver = new BrowserFactory().CustomDriver(incognito);
            
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);
            driver.Manage().Window.Maximize();
        }

        public void LauchBrowser()
        {
            driver = new BrowserFactory().CustomDriver();

            driver.Manage().Timeouts().PageLoad.Add(TimeSpan.FromSeconds(60));
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);
            driver.Manage().Window.Maximize();
        }

        public void GoTo(String val)
        {
            Thread.Sleep(2000);
            driver.Navigate().GoToUrl(Db().GetField(val + "URL"));
        }
        public MongoDbManager Db()
        {
            return new MongoDbManager();
        }

        public ConfigurationManager Config()
        {
            return new ConfigurationManager();
        }

        public String NewUser(String val)
        {
            return String.Format(Db().GetField("Username"), val);
        }

        public String Rand()
        {
            return new Random().Next(0, 9999).ToString();
        }

        public String Rng(int startRange, int endRange)
        {
            return new Random().Next(startRange, endRange).ToString();
        }

        protected void Refresh()
        {
            driver.Navigate().Refresh();
        }

        public void Dispose()
        {
            driver.Quit();
        }      
    }
}
